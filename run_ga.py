import argparse
import matplotlib.pyplot as plt
import logging
import sys
import os

from src.chp import LinearCHP
from src.mes import MESWithMixedTES, MESMetaheuristic
from src.solver import Solver, GASolver
from src.results import save_result, ResultGroup


logger = logging.getLogger(__name__)


def get_timesteps(start, duration):
    return list(range(start, start+duration))


def set_logger(filename):
    base_logger = logging.getLogger('')
    base_logger.setLevel(logging.INFO)
    formatter = logging.Formatter(
        fmt='%(asctime)s:%(levelname)s:%(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S')
    if filename is not None:
        file_handler = logging.FileHandler(filename)
        file_handler.setFormatter(formatter)
        base_logger.addHandler(file_handler)
    stream_handler = logging.StreamHandler(sys.__stdout__)
    stream_handler.setFormatter(formatter)
    base_logger.addHandler(stream_handler)


def log_args(args):
    logger.info('Command-line arguments:')
    for arg in vars(args):
        logger.info(f'  {arg}: {getattr(args, arg)}')


def solve_QP(args):
    dt = get_timesteps(args.time[0], args.time[1])
    chp = LinearCHP()
    mes = MESWithMixedTES(dt=dt, chp=chp, radius=args.r)
    mes.m.setParam('MIPGap', args.g)
    solver = Solver(mes, epsilon_gap=1e-2)
    solver.init_variables()
    solver.set_constraints()
    result = solver.optimize(prioritizeRES=True)
    if args.p:
        result.plot()
        plt.show()
    return result


def solve_GA(args):
    dt = get_timesteps(args.time[0], args.time[1])
    logging.getLogger('src.solver.heuristic').addFilter(lambda record: False)
    # if linearization_pieces == 1:
        # chp = LinearCHP()
    # else:
        # chp = AdaptedCHP(N=linearization_pieces)
    chp = LinearCHP()
    mes = MESMetaheuristic(dt=dt, chp=chp)
    solver = GASolver(
        mes, epsilon_gap=1e-2, nbr_iterations=args.iterations, size=args.z,
        seed=args.seed, sigma_factor=args.sigma, elite_percent=args.elites,
        crossover_weights=(args.crossover, 1 - args.crossover),
        nbr_no_improvement=args.no_improvement, nodes=args.nodes)
    solver.init_variables()
    solver.set_constraints()
    result = solver.optimize(prioritizeRES=True)
    if args.p:
        result.plot()
        plt.show()
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Solve thermal storage model')
    parent = argparse.ArgumentParser(
        add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parent.add_argument(
        '--time', '-t', type=int, nargs=2, default=[0, 8760],
        metavar=('START', 'DURATION'), help='start time and duration')
    parent.add_argument(
        '-o', type=str, metavar='PATH', help='output save file')
    parent.add_argument(
        '-l', type=str, metavar='PATH', help='output log file')
    parent.add_argument(
        '-p', action='store_true', help='plot result')

    subparsers = parser.add_subparsers(required=True)

    parser_QP = subparsers.add_parser(
        'QP', help='solve using a QP', parents=[parent],
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_QP.set_defaults(func=solve_QP)
    parser_QP.add_argument(
        '-r', type=float, default=2.5, help='radius of the storage')
    parser_QP.add_argument(
        '-g', type=float, default=0, help='gap of the solver')

    parser_GA = subparsers.add_parser(
        'GA', help='solve using a GA', parents=[parent],
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_GA.set_defaults(func=solve_GA)
    parser_GA.add_argument(
        '--iterations', '-i', type=int, default=100, help='number of iterations')
    parser_GA.add_argument(
        '-z', type=int, default=30, help='size of the population')
    parser_GA.add_argument(
        '--seed', type=float, default=None, help='random seed')
    parser_GA.add_argument(
        '--sigma', type=float, default=1.0, help='sigma factor')
    parser_GA.add_argument(
        '--crossover', type=float, default=0.67, help='weight of the best parent')
    parser_GA.add_argument(
        '--elites', type=float, default=0.2, help='percentage of elites')
    parser_GA.add_argument(
        '--no_improvement', type=int, default=None, help='nbr of iterations without improvement')
    parser_GA.add_argument(
        '--nodes', type=int, default=1, help='number of storage nodes')

    args = parser.parse_args()
    with open(os.devnull, 'w') as sys.stdout:
        set_logger(args.l)
        log_args(args)
        result = args.func(args)
        group = ResultGroup([result], duration=result.duration)
        if args.o is not None:
            save_result(args.o, group)
