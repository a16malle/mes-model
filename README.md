# MES model

Scripts to optimize the design and operation of a multi-energy system.

## Requirements

This project uses the [Gurobi](https://www.gurobi.com/) solver.
The Python library `gurobipy` needs to be installed with a valid licence to solve the models.

Install the required packages using Pip.
```bash
pip install -r requirements.txt
```

## Usage

Optimize a model using `run.py`.
The available options can be viewed using the `--help` argument.

For example, to calculate 10 solutions:
```bash
python run.py --number 10 --dir output_directory
```

The results can be displayed using the `display_result.py` script.
The available options can be viewed using the `--help` argument.

For example, to show various info and plot the Pareto front:
```bash
python display_result.py show -P -vv result.pkl.gz
```

