import argparse
import matplotlib.pyplot as plt

from src.results import load_result
from src.utils import (
    bcolors as ts, show_comparison, plot_pareto, plot_result, plot_bisector,
    plot_all_boxplots, display_capacity_factor, set_language, translate as _)


def single(args):
    """Display the results for a single result file."""
    results = load_result(args.filename)
    print(f'{ts.HEADER}###### Results ######{ts.ENDC}')
    print(f'Total duration: {results.duration:.3f}s')
    if args.v >= 1:
        for i, r in enumerate(results.results):
            print(f'{ts.UNDERLINE}Solution {i+1}/{len(results.results)}{ts.ENDC}')
            print(f'Duration: {r.duration:.3f}s')
            print(f'\n    {ts.UNDERLINE}Energy balance{ts.ENDC}')
            r.print_energy_balance()
            if args.v >= 2:
                print(f'\n    {ts.UNDERLINE}Capacity and statistics on the operation{ts.ENDC}')
                r.display(objectives=False)
            if args.c:
                print(f'\n    {ts.UNDERLINE}Capacity factor{ts.ENDC}')
                display_capacity_factor(r)
            print('')
    figure = None
    if args.P:
        figure = plt.figure()
        ax = plt.subplot(111)
        results.plot_pareto(ax=ax)
    elif args.p is not None:
        plot_result = args.p - 1 if args.p is not None else None
        figure = results.results[plot_result].plot(monotone=args.m, environment=False)
    if figure is not None:
        if args.s is not None:
            figure.savefig(args.s)
        else:
            plt.show()


def compare(args):
    """Compare two results files."""
    optimal = load_result(args.optimal)
    heuristic = load_result(args.heuristic)

    # Print comparison of the results
    gen = show_comparison(optimal, heuristic, verbosity=args.v)
    for ro, ra in gen:
        if args.c:
            print(f'\n    {ts.UNDERLINE}Capacity factor{ts.ENDC}')
            display_capacity_factor(ro, ra, labels=['Optimal', 'Heuristic'])

    # plt.style.use('seaborn')
    figure = None

    if args.P:
        # Plot Pareto fronts
        figure = plot_pareto(optimal, heuristic, legend=[_('Solver'), _('Heuristic')])
    elif args.p is not None:
        # Plot solution
        to_plot = args.p - 1
        ro = optimal.results[to_plot]
        rh = heuristic.results[to_plot]
        figure = plot_result(ro, rh, legend=[_('Solver'), _('Heuristic')])
        figure.tight_layout()
    elif args.b is not None:
        # Plot bisector
        to_plot = args.b - 1
        ro = optimal.results[to_plot]
        rh = heuristic.results[to_plot]
        figure = plot_bisector(ro, rh, labels=['Optimal', 'Heuristic'])
    elif args.boxplot:
        # Plot boxplots
        figure = plot_all_boxplots(optimal, heuristic)

    if figure is not None:
        if args.s is not None:
            figure.savefig(args.s)
        else:
            plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Display results')
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument('-s', type=str, metavar='PATH', help='Save figure to PATH')
    parent_parser.add_argument(
        '-m', help='display the results as monotone functions', action='store_true')
    parent_parser.add_argument('-v', help='verbosity', default=0, action='count')
    parent_parser.add_argument('--fr', help='translate in french', action='store_true')

    subparsers = parser.add_subparsers(required=True)

    # Single file
    parser_single = subparsers.add_parser(
        'show', help='display result for a single file', parents=[parent_parser])
    parser_single.set_defaults(func=single)
    group = parser_single.add_mutually_exclusive_group()
    group.add_argument('-P', help='plot Pareto front', action='store_true')
    group.add_argument('-p', type=int, metavar='NBR', help='plot result NBR')
    parser_single.add_argument(
        '-c', help='plot capacity factor of solution NBR', action='store_true')
    parser_single.add_argument('filename', type=str, help='result of the MILP')

    # Compare two files
    parser_compare = subparsers.add_parser(
        'compare', help='compare two results', parents=[parent_parser])
    parser_compare.set_defaults(func=compare)
    group = parser_compare.add_mutually_exclusive_group()
    group.add_argument('-P', help='plot Pareto front', action='store_true')
    group.add_argument('-p', type=int, metavar='NBR', help='plot result NBR')
    group.add_argument('-b', type=int, metavar='NBR', help='plot bisector figure for result NBR')
    group.add_argument(
        '--boxplot', help='plot boxplots for all the solutions', action='store_true')
    parser_compare.add_argument('-c', help='plot capacity factors', action='store_true')
    parser_compare.add_argument('optimal', type=str, help='result of the MILP')
    parser_compare.add_argument('heuristic', type=str, help='result of the heuristic')
    args = parser.parse_args()

    if args.fr:
        set_language('fr')

    args.func(args)
