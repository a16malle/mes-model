#!/usr/bin/env python
# coding: utf-8
import argparse
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import ttest_ind


FNAME = 'out/tuning/server/tuning_3days/01-winter.out.json.gzip'
# FNAME = '../out/tuning/tests/09.out.json.gzip'


# set the max columns to none
pd.set_option('display.max_columns', None)

pd.set_option('display.max_colwidth', 3000)
pd.set_option('display.expand_frame_repr', False)

keys = ['size', 'nbr_iterations', 'nbr_no_improvement',
        'crossover_best_weight', 'sigma_factor', 'elite_percent']


def load_file(filename):
    result = pd.read_json(filename, precise_float=True)
    return result


def group(df):
    grouped = df.groupby(keys).agg(
            duration=('duration', 'mean'),
            fitness=('fitness', 'mean'),
            count=('seed', 'count'))
    return grouped


def get_best(df, thresh=0.05):
    test = df.groupby(keys).agg(list).reset_index()
    best = set([0])

    for i, row in test.iterrows():
        # print(i)
        if i == 0:
            continue
        copy = list(best)
        # statistic positive if row is better than best set
        # pvalue greater than the threshold means that a solution is not better than the other
        ttests = [ttest_ind(row.fitness, test.iloc[j].fitness, equal_var=False) for j in copy]
        if all([t.statistic > 0 and t.pvalue <= thresh or t.pvalue > thresh for t in ttests]):
            # print(f'  Adding {i}')
            best.add(i)
        for j, ttest in zip(copy, ttests):
            if ttest.statistic > 0 and ttest.pvalue <= thresh:
                # if the current row is better than j, remove j
                # print(f'  Removing {j}, too low: stat={-ttest.statistic:.2f}, pvalue={ttest.pvalue:.1e}')
                best.remove(j)
    return best


def plot_final(df, final):
    values = df.merge(
        final, on=['crossover_best_weight', 'sigma_factor', 'elite_percent'],
        how='left', indicator=True
    ).query("_merge == 'both'")
    ax = values.boxplot(column='fitness_x', by=['crossover_best_weight', 'sigma_factor', 'elite_percent'], fontsize='small', showmeans=True)
    ax.set_title('Boxplot of best hyper-parameters')
    ax.set_xlabel('(crossover weight, sigma factor, elite percent)')
    ax.set_ylabel('fitness')
    plt.suptitle('')
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")


def plot_all(df, best=None):
    ax = df.boxplot(column='fitness', by=['crossover_best_weight', 'sigma_factor', 'elite_percent'], fontsize='x-small')
    ax.set_title('Boxplot of all hyper-parameters')
    ax.set_xlabel('(crossover weight, sigma factor, elite percent)')
    ax.set_ylabel('fitness')
    if best is not None:
        for i in best:
            ax.axvspan(i+0.5, i+1.5, color='gray', alpha=0.1)  # highlight the best values from the t-test
    plt.suptitle('')
    plt.setp(ax.get_xticklabels(), rotation=70, ha="right", rotation_mode="anchor")


def plot_or_save(filename=None):
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)


def main(filename, out_best=None, out_all=None):
    result = load_file(filename)
    grouped = group(result)
    print(grouped.sort_values(by='fitness'))

    # Display factors boxplots
    # result.boxplot(column='fitness', by='crossover_best_weight')
    # result.boxplot(column='fitness', by='sigma_factor')
    # result.boxplot(column='fitness', by='elite_percent')

    best = get_best(result)
    final = grouped.iloc[list(best)]
    print('\nFinal best results:', final)
    plot_final(result, final)
    plot_or_save(out_best)
    plot_all(result, best)
    plot_or_save(out_all)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analyse results to find best')
    parser.add_argument('filename', type=str, help='results file')
    parser.add_argument('--best', type=str, help='output file')
    parser.add_argument('--all', type=str, help='output file')

    args = parser.parse_args()

    main(args.filename, args.best, args.all)
