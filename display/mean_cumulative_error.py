# flake8: noqa
import argparse
import tempfile
import os
from os.path import splitext
from zipfile import ZipFile
import matplotlib.pyplot as plt
import json
import numpy as np

from src.results import ResultGroup
from src.utils import sorted_alphanumeric, set_language, translate as _
from src.parameters import a, b, c


def f_F_g(P_e, P_nom):
    n_e = a + b*P_e/P_nom + c*(P_e/P_nom)**2
    # q = n_th_CHP*(1 - n_e)*p/n_e
    return P_e/n_e


parser = argparse.ArgumentParser(description='Display Pareto fronts')
parser.add_argument('archives', metavar='archive', type=str, nargs='+',
                    help='archive of the results')
parser.add_argument('--out', type=str, help='output file')
parser.add_argument('--nodisplay', help="don't display the figures", action='store_true')
parser.add_argument('--fr', help='language of the figures', action='store_true')

args = parser.parse_args()

default_figsize = plt.rcParams['figure.figsize']
figsize = 1.5*default_figsize[1], 0.75*default_figsize[1]
fig, ax = plt.subplots(1, 1, figsize=figsize)

if args.fr:
    set_language('fr')

values = {}

sorted_archives = sorted_alphanumeric(args.archives)

for archive in sorted_archives:
    print(archive)
    results = ResultGroup.load_from_zip(archive)
    arguments = results.arguments
    x, y = [], []
    cumulative_error = 0
    cumulative_error_up = 0
    cumulative_error_low = 0
    for r in results.results:
        F_g_real = f_F_g(r.operation.P_e_CHP, r.design.P_CHP_nom[0])
        F_g_appr = r.operation.F_g_CHP

        # Old version. Had an error as the absolute value of the sums is not
        # the same as the sum of the absolute values.
        # The difference can be positive or negative.
        # error = abs(np.sum(F_g_appr) - np.sum(F_g_real))/np.sum(F_g_real)

        error = np.sum(np.abs(F_g_appr - F_g_real))/np.sum(F_g_real)
        cumulative_error += error
        if error > 0:
            cumulative_error_up += error
        else:
            cumulative_error_low += error

    mean_cum_error = cumulative_error/len(results.results)

    method = arguments['linearization']

    nbr_triangles = 0
    if method == 'triangle':
        # Number of triangles used. We ignore the half where there is no triangles.
        nbr_triangles = (arguments['N'] - 1)*(arguments['M'] - 1)
    elif method == 'other':
        nbr_triangles = arguments['N'] - 1

    if method not in values:
        values[method] = ([], [], [], [])
    values[method][0].append(nbr_triangles)
    values[method][1].append(mean_cum_error)
    values[method][2].append(cumulative_error_up)
    values[method][3].append(cumulative_error_low)

    # label = ''
    # if method is None:
        # label = 'No linearization'
    # if method == 'other':
        # label = 'Other ({})'.format(arguments['N'])
    # if method == 'triangle':
        # label = 'Triangle ({}x{})'.format(arguments['N'], arguments['M'])
    # plt.plot(x, y, '-+', label=label)

for method, (x, y, y_up, y_low) in values.items():
    print(method, x, y, y_up, y_low)
    labels = {
        None: _('Constant efficiency'),
        'triangle': _('Triangle linearization'),
        'other': _('Adapted linearization')
    }
    plt.plot(x, y, 'x-', label=labels.get(method))
    # plt.plot(x, y_up, 'x-', label=labels.get(method))
    # plt.plot(x, y_low, 'x-', label=labels.get(method))
    # yerr = [], []
    # for v, v_u, v_l in zip(y, y_up, y_low):
        # yerr[0].append(v - v_l)
        # yerr[1].append(v + v_u)
    # plt.errorbar(x, y, yerr=yerr, capsize=4, label=labels.get(method))

plt.legend()
plt.xlabel(_('Number of triangles'))
plt.ylabel(_('Mean cumulative error'))
plt.grid(True)

from matplotlib import ticker
formatter = ticker.ScalarFormatter(useMathText=True)
formatter.set_scientific(True) 
formatter.set_powerlimits((-1,1)) 
ax.yaxis.set_major_formatter(formatter) 

if args.out is not None:
    plt.savefig(args.out, bbox_inches='tight')

if not args.nodisplay:
    plt.show()
