from statistics import mean

from src.parameters import T, G, L_e, L_h


days = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

for i, d in enumerate(days):
    cum = sum(days[0:i])
    s = cum*24
    e = (cum + d)*24
    print(f'Mois {i+1}: L_e={mean(L_e[s:e]):.0f}, L_h={mean(L_h[s:e]):.0f}, T={mean(T[s:e]):.1f}, G={mean(G[s:e]):.2f}')
