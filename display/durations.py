import argparse
import matplotlib.pyplot as plt

from src.results import ResultGroup
from src.utils import sorted_alphanumeric, set_language, translate as _


parser = argparse.ArgumentParser(description='Display durations')
parser.add_argument('archives', metavar='archive', type=str, nargs='+',
                    help='archive of the results')
parser.add_argument('--out', type=str, help='output file')
parser.add_argument('--nodisplay', help="don't display the figures", action='store_true')
parser.add_argument('--fr', help='language of the figures', action='store_true')

args = parser.parse_args()

default_figsize = plt.rcParams['figure.figsize']
figsize = 1.5*default_figsize[1], 0.75*default_figsize[1]
fig = plt.figure(figsize=figsize)

if args.fr:
    set_language('fr')

values = {}

sorted_archives = sorted_alphanumeric(args.archives)

for archive in sorted_archives:
    print(archive)
    results = ResultGroup.load_from_zip(archive)
    arguments = results.arguments

    method = arguments['linearization']
    nbr_triangles = 0
    if method == 'triangle':
        # Number of triangles used. We ignore the half where there is no triangles.
        nbr_triangles = (arguments['N'] - 1)*(arguments['M'] - 1)
    elif method == 'other':
        nbr_triangles = arguments['N'] - 1

    if method not in values:
        values[method] = ([], [])

    values[method][0].append(nbr_triangles)
    values[method][1].append(results.duration)

print('times:', values)
for method, (x, y) in values.items():
    labels = {
        None: _('Constant efficiency'),
        'triangle': _('Triangle linearization'),
        'other': _('Adapted linearization')
    }
    plt.plot(x, y, 'x-', label=labels.get(method))

plt.legend()
plt.yscale('log')
plt.xlabel(_('Number of triangles'))
plt.ylabel(_('Time (s)'))
plt.grid(True)

if args.out is not None:
    plt.savefig(args.out, bbox_inches='tight')

if not args.nodisplay:
    plt.show()
