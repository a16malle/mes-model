import argparse
import matplotlib.pyplot as plt

from src.results import ResultGroup
from src.utils import sorted_alphanumeric, set_language, translate as _


parser = argparse.ArgumentParser(description='Display Pareto fronts')
parser.add_argument(
    'archives', metavar='archive', type=str, nargs='+', help='archive of the results')
parser.add_argument('--out', type=str, help='output file')
parser.add_argument('--nodisplay', help="don't display the figures", action='store_true')
parser.add_argument('--fr', help='language of the figures', action='store_true')
parser.add_argument('--annotate', type=int, nargs='+', help='annotate points')

args = parser.parse_args()
print(args.annotate)

if args.fr:
    set_language('fr')

fig, ax = plt.subplots()

sorted_archives = sorted_alphanumeric(args.archives)

for archive in sorted_archives:
    print(archive)
    results = ResultGroup.load_from_zip(archive)
    x, y = [], []
    for r in results.results:
        x.append(r.ACR)
        y.append(r.RES)

    label = ''
    if results.arguments['linearization'] is None:
        label = 'No linearization'
    if results.arguments['linearization'] == 'other':
        label = 'Other ({})'.format(results.arguments['N'])
    if results.arguments['linearization'] == 'triangle':
        label = 'Triangle ({}x{})'.format(results.arguments['N'], results.arguments['M'])
    plt.plot(x, y, 'o--', label=label)
    for annotation in args.annotate:
        cx = x[annotation - 1]
        cy = y[annotation - 1]
        plt.annotate(
            f'({annotation})',
            xy=(cx, cy), xycoords='data',
            xytext=(4, 4), textcoords='offset points'
        )

# plt.legend()
plt.xlabel(_(r'$ATCR$'))
plt.ylabel(_(r'$\tau_{RES}$'))
plt.grid(True)

if args.out is not None:
    plt.savefig(args.out, bbox_inches='tight')

if not args.nodisplay:
    plt.show()
