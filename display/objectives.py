# flake8: noqa
import argparse
import tempfile
import os
from os.path import splitext
from zipfile import ZipFile
import matplotlib.pyplot as plt
import json
import numpy as np
from statistics import mean

from src.results import ResultGroup
from src.utils import sorted_alphanumeric, set_language, translate as _


parser = argparse.ArgumentParser(description='Display Pareto fronts')
parser.add_argument(
    'archives', metavar='archive', type=str, nargs='+', help='archive of the results')
parser.add_argument('--out', type=str, help='output file')
parser.add_argument('--nodisplay', help="don't display the figures", action='store_true')
parser.add_argument('--fr', help='language of the figures', action='store_true')

args = parser.parse_args()

if args.fr:
    set_language('fr')

fig = plt.figure(figsize=[1.2*6.4*1.4, 1.2*4.8*0.5])

objectives1 = {}
objectives2 = {}

sorted_archives = sorted_alphanumeric(args.archives)

for archive in sorted_archives:
    # print(archive)
    results = ResultGroup.load_from_zip(archive)
    arguments = results.arguments
    o1, o2 = [], []
    for r in results.results:
        o1.append(r.ACR)
        o2.append(r.RES)

    method = arguments['linearization']

    nbr_triangles = 0
    if method == 'triangle':
        # Number of triangles used. We ignore the half where there is no triangles.
        nbr_triangles = (arguments['N'] - 1)*(arguments['M'] - 1)
    elif method == 'other':
        nbr_triangles = arguments['N'] - 1

    if method not in objectives1:
        objectives1[method] = ([], [], [])
    objectives1[method][0].append(nbr_triangles)
    objectives1[method][1].append(np.mean(o1))
    objectives1[method][2].append(np.std(o1))

    if method not in objectives2:
        objectives2[method] = ([], [], [])
    objectives2[method][0].append(nbr_triangles)
    objectives2[method][1].append(np.mean(o2))
    objectives2[method][2].append(np.std(o2))

    # label = ''
    # if method is None:
        # label = 'No linearization'
    # if method == 'other':
        # label = 'Adapted ({})'.format(arguments['N'])
    # if method == 'triangle':
        # label = 'Triangle ({}x{})'.format(arguments['N'], arguments['M'])
    # plt.plot(x, y, '-+', label=label)

axs = fig.add_subplot(1, 2, 1)
for method, (x, y, yerr) in objectives1.items():
    labels = {
        None: _('Constant efficiency'),
        'triangle': _('Triangle linearization'),
        'other': _('Adapted linearization')
    }
    axs.errorbar(x, y, yerr=yerr, fmt='x-', capsize=3, label=labels.get(method))
    axs.set_xlabel(_('Number of triangles'))
    axs.set_ylabel(_(r'$ATCR$'))
    plt.legend()
    plt.grid(True)

axs = fig.add_subplot(1, 2, 2)
for method, (x, y, yerr) in objectives2.items():
    labels = {
        None: _('Constant efficiency'),
        'triangle': _('Triangle linearization'),
        'other': _('Adapted linearization')
    }
    axs.errorbar(x, y, yerr=yerr, fmt='x-', capsize=3, label=labels.get(method))
    axs.set_xlabel(_('Number of triangles'))
    axs.set_ylabel(_(r'$\tau_{RES}$'))
    plt.grid(True)

# plt.legend()
# plt.xlabel('Number of triangles')
# plt.ylabel('Objective function')
# plt.grid(True)

for method in labels:
    _, ACR_mean, ACR_std = objectives1[method]
    _, RES_mean, RES_std = objectives2[method]
    # print(method, 'ACR std :', ' '.join(format(v, '.3f') for v in ACR_std))
    # print(method, 'RES std :', ' '.join(format(v, '.3f') for v in RES_std))
    print(method, 'ACR mean :', ' '.join(format(v, '.1f') for v in ACR_mean))
    print(method, 'RES mean :', ' '.join(format(v, '.1f') for v in RES_mean))
    print(f'  ACR std mean: {mean(ACR_std):.3f}, RES std mean: {mean(RES_std):.3f}')


if args.out is not None:
    plt.savefig(args.out, bbox_inches='tight')

if not args.nodisplay:
    plt.show()
