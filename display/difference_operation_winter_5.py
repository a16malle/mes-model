import matplotlib.pyplot as plt

from src.results import load_result
from src.utils import translate as _, set_language

TO_PLOT = 4  # Fifth solution

set_language('fr')

optimal = load_result('out/server_heuristic/optimal-1080+168_10_10.pkl.gz')
heuristic = load_result('out/server_heuristic/approximate-1080+168_10_10.pkl.gz')

ro = optimal.results[TO_PLOT]
rh = heuristic.results[TO_PLOT]

default_figsize = plt.rcParams['figure.figsize']
figsize = 1.1*default_figsize[0], 1.1*default_figsize[1]

fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, sharey='col', squeeze=False, figsize=figsize)

ax_o = axs[0, 0]
ax_o.set_title('Solveur {}'.format(_('(electricity)')))
ro._plot_elec(ax_o, colors=True, legend=False)

ax_h = axs[1, 0]
ax_h.set_title('Heuristique {}'.format(_('(electricity)')))
rh._plot_elec(ax_h, colors=True, legend=True)

ax_o.set_xlim(5, 115)
ax_o.set_xticks(list(range(10, 115, 10)))
ax_o.set_ylim(500, 850)

fig.tight_layout()
fig.savefig('/home/antoine/ownCloud/1 Projects/Doctorat/5 Documents/2023-04 Thèse/Manuscrit/Figures/difference_operation_zoom.pdf')
plt.show()
