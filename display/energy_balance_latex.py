import argparse

from src.results import load_result
from src.utils import print_energy_balance_latex


def compare(args):
    """Compare two results files."""
    for d in args.data:
        print(d)
    data = [load_result(d) for d in args.data]
    print('\\hline')
    for i in range(len(data[0].results)):
        results = [d.results[i] for d in data]
        if args.labels:
            labels = [f'{args.labels[j]} ({i+1})' for j in range(len(args.labels))]
        else:
            labels = [f'Data {j+1} ({i+1})' for j in range(len(args.data))]
        print_energy_balance_latex(*results, labels=labels, diff=args.diff)
        if len(args.data) > 1:
            print('\\hline')
    if len(args.data) == 1:
        print('\\hline')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Display results')
    parser.add_argument('data', nargs='+', type=str, help='result of the MILP')
    parser.add_argument('--diff', action='store_true', help='highlight differences')
    parser.add_argument('--labels', nargs='+', type=str, help='labels of the data')
    args = parser.parse_args()
    print(args.data)
    compare(args)
