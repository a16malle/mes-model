import argparse
import matplotlib.pyplot as plt
import numpy as np

from src.results import ResultGroup
from src.utils import sorted_alphanumeric, set_language, translate as _
from src.parameters import (a, b, c)


def f_F_g(P_e, P_nom):
    n_e = a + b*P_e/P_nom + c*(P_e/P_nom)**2
    # q = n_th_CHP*(1 - n_e)*p/n_e
    return P_e/n_e


parser = argparse.ArgumentParser(description='Display Pareto fronts')
parser.add_argument('archives', metavar='archive', type=str, nargs='+',
                    help='archive of the results')
parser.add_argument('--out', type=str, help='output file')
parser.add_argument('--nodisplay', help="don't display the figures", action='store_true')
parser.add_argument('--fr', help='language of the figures', action='store_true')

args = parser.parse_args()

if args.fr:
    set_language('fr')

values = {}

sorted_archives = sorted_alphanumeric(args.archives)

for archive in sorted_archives:
    print(archive)
    results = ResultGroup.load_from_zip(archive)
    arguments = results.arguments
    values_list = []
    for r in results.results:
        P_e = r.operation.P_e_CHP
        P_nom = r.design.P_CHP_nom[0]
        F_g_real = f_F_g(r.operation.P_e_CHP, r.design.P_CHP_nom[0])
        F_g_appr = r.operation.F_g_CHP
        efficiencies = P_e/F_g_appr
        # efficiencies[np.isnan(efficiencies)] = 0
        efficiencies = list(efficiencies[~np.isnan(efficiencies)])
        # print(efficiencies.describe())
        efficiencies = list(efficiencies)
        values_list.append(efficiencies)

    method = arguments['linearization']

    label = ''
    if method is None:
        label = _('No linearization')
    if method == 'other':
        label = _('Adapted linearization') + ' ({})'.format(arguments['N']-1)
    if method == 'triangle':
        label = _('Triangle linearization') + ' ({}x{})'.format(arguments['N']-1, arguments['M']-1)

    values[label] = values_list

fig, axs = plt.subplots(len(values), sharey=True, squeeze=False, figsize=[6.4, 4.8*0.8*len(values)])
axs[0, 0].set_ylim(a, a + b + c)

for i, k in enumerate(values):
    # axs[i].boxplot(values[k], showfliers=True, whis=(0, 100))
    axs[i, 0].boxplot(values[k], showfliers=True)
    axs[i, 0].set_title(k)
    # axs[i, 0].set_xlabel('Run')
    axs[i, 0].set_xlabel(_(r'Solutions $s$ of the Pareto set $\mathcal{S}$'))
    axs[i, 0].set_ylabel(_(r'$\eta_{e,CHP}$'))
    axs[i, 0].grid(True)

if args.out is not None:
    plt.savefig(args.out, bbox_inches='tight', dpi=150, pad_inches=0.075)

if not args.nodisplay:
    plt.show()
