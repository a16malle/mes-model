import argparse
import matplotlib.pyplot as plt
from statistics import mean

import src.parameters as p
from src.utils import set_language, translate as _

parser = argparse.ArgumentParser(description='Display time series parameters')
parser.add_argument('--start', type=int, default=0, help='start time')
parser.add_argument('--duration', type=int, default=8760, help='duration')
parser.add_argument('--fr', action='store_true', help='translate in french')
parser.add_argument('-s', type=str, help='save figure to path')
parser.add_argument('--scale', type=float, nargs=2, default=(0.7, 0.7), help='scale of figure')
parser.add_argument('--plot', nargs='*', default=['demand', 'environment'], help='what to plot')

args = parser.parse_args()

if args.fr:
    set_language('fr')

dt = list(range(args.start, args.start + args.duration))
idx = list(range(len(dt)))

nbr = len(args.plot)
sx, sy = args.scale

default_figsize = plt.rcParams['figure.figsize']
figsize = sx*nbr*default_figsize[0], sy*default_figsize[1]
fig, axs = plt.subplots(nrows=2, ncols=nbr, sharex='all', figsize=figsize, squeeze=False)

L_e = [p.L_e[t] for t in dt]
L_h = [p.L_h[t] for t in dt]
T = [p.T[t] for t in dt]
G = [p.G[t] for t in dt]

print(f'Period {args.start}+{args.duration}')
print(f'  L_e: min {min(L_e):.0f}, max {max(L_e):.0f}, mean {mean(L_e):.0f}')
print(f'  L_h: min {min(L_h):.0f}, max {max(L_h):.0f}, mean {mean(L_h):.0f}')
print(f'  T: min {min(T):.1f}, max {max(T):.1f}, mean {mean(T):.1f}')
print(f'  G: min {min(G)*1000:.0f}, max {max(G)*1000:.0f}, mean {mean(G)*1000:.0f}')

i = 0

if 'demand' in args.plot:
    axs[0, i].plot(idx, L_e)
    axs[0, i].set_ylabel(_('Electricty demand (kW)'))
    axs[1, i].plot(idx, L_h)
    axs[1, i].set_ylabel(_('Heat demand (kW)'))

    axs[0, i].grid(True)
    axs[1, i].grid(True)

    axs[1, i].set_xlim(idx[0], idx[-1])
    axs[1, i].set_xlabel(_('Hours (h)'))
    i += 1

if 'environment' in args.plot:
    axs[0, i].plot(idx, T)
    axs[0, i].set_ylabel(_('Temperature (°C)'))
    axs[1, i].plot(idx, G)
    axs[1, i].set_ylabel(_('Solar irradiation (kW/m²)'))

    axs[0, i].grid(True)
    axs[1, i].grid(True)

    axs[1, i].set_xlim(idx[0], idx[-1])
    axs[1, i].set_xlabel(_('Hours (h)'))

if args.s is not None:
    fig.savefig(args.s)
else:
    plt.show()
