# flake8: noqa
from src.results import ResultGroup
from src.utils import load_result, show_comparison

optimal = ResultGroup.load_from_zip('/home/antoine/data/doctorat/mes_model/server/results/0-8760-gap0.001/0+8760_other_16_None_10_20220207T140433.zip')
optimal.results.reverse()

heuristic = load_result('out/server_heuristic/approximate-0+8760_10_10.pkl.gz')

parser = argparse.ArgumentParser(description='Compare old and new results')

gen = show_comparison(optimal, heuristic, verbosity=args.v)
for ro, ra in gen:
    if args.c:
        print(f'\n    {ts.UNDERLINE}Capacity factor{ts.ENDC}')
        display_capacity_factor(ro, ra, labels=['Optimal', 'Heuristic'])
figure = None
if args.P:
    figure = plot_pareto(optimal, heuristic, legend=['Optimal', 'Heuristic'])
elif args.p is not None:
    to_plot = args.p - 1
    ro = optimal.results[to_plot]
    rh = heuristic.results[to_plot]
    figure = plot_result(ro, rh, legend=['Optimal', 'Heuristic'])
elif args.b is not None:
    to_plot = args.b - 1
    ro = optimal.results[to_plot]
    rh = heuristic.results[to_plot]
    figure = plot_bisector(ro, rh, labels=['Optimal', 'Heuristic'])
elif args.boxplot:
    figure = plot_all_boxplots(optimal, heuristic)
if figure is not None:
    if args.s is not None:
        figure.savefig(args.s)
    else:
        plt.show()
