import argparse
import matplotlib.pyplot as plt
import math
from statistics import median

from src.results import ResultGroup
from src.utils import sorted_alphanumeric, set_language, translate as _


parser = argparse.ArgumentParser(description='Display Pareto fronts')
parser.add_argument(
    'archives', metavar='archive', type=str, nargs='+', help='archive of the results')
parser.add_argument('--out', type=str, help='output file')
parser.add_argument('--nodisplay', help="don't display the figures", action='store_true')
parser.add_argument('--fr', help='language of the figures', action='store_true')

args = parser.parse_args()

default_figsize = plt.rcParams['figure.figsize']
figsize = 1.5*default_figsize[1], 0.75*default_figsize[1]
fig = plt.figure(figsize=figsize)

if args.fr:
    set_language('fr')

distances = {}

sorted_archives = sorted_alphanumeric(args.archives)

for archive in sorted_archives:
    # print(archive)
    results = ResultGroup.load_from_zip(archive)
    arguments = results.arguments
    x, y = [], []
    for r in results.results:
        x.append(r.ACR)
        y.append(r.RES)

    method = arguments['linearization']

    nbr_triangles = 0
    if method == 'triangle':
        # Number of triangles used. We ignore the half where there is no triangles.
        nbr_triangles = (arguments['N'] - 1)*(arguments['M'] - 1)
    elif method == 'other':
        nbr_triangles = arguments['N'] - 1

    # Calculate the distance from the origin
    # if not (method == 'other' and nbr_triangles == 1):
    distance = 0
    for vx, vy in zip(x, y):
        distance += math.sqrt(vx**2 + vy**2)
    distance /= len(x)

    if method not in distances:
        distances[method] = ([], [])
    distances[method][0].append(nbr_triangles)
    distances[method][1].append(distance)

    # label = ''
    # if method is None:
        # label = 'No linearization'
    # if method == 'other':
        # label = 'Other ({})'.format(arguments['N'])
    # if method == 'triangle':
        # label = 'Triangle ({}x{})'.format(arguments['N'], arguments['M'])
    # plt.plot(x, y, '-+', label=label)

for method, (x, y) in distances.items():
    labels = {
        None: _('Constant efficiency'),
        'triangle': _('Triangle linearization'),
        'other': _('Adapted linearization')
    }
    plt.plot(x, y, 'x-', label=labels.get(method))

plt.legend()
plt.xlabel(_('Number of triangles'))
plt.ylabel(_('Mean Distance'))
plt.grid(True)

for method in distances:
    d_list = distances[method][1]
    print(method, ':', ' '.join(format(d, '.3f') for d in d_list))
    print(f'  median: {median(d_list):.3f}')

if args.out is not None:
    plt.savefig(args.out, bbox_inches='tight')

if not args.nodisplay:
    plt.show()
