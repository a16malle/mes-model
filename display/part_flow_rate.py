import matplotlib.pyplot as plt

from src.solver import GASolver
from src.mes import MESMetaheuristic
from src.chp import LinearCHP
import src.parameters as p

SIZE = 1
START = 1194  # 1206
DURATION = 24
FIGBASE = '/home/antoine/ownCloud/1 Projects/Doctorat/5 Documents/2023-04 Thèse/Manuscrit/Figures/'

# Get default colors list
prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

dt = list(range(START, START+DURATION))
it = list(range(len(dt)))


def show(solutions, filename=None):
    # Generate figures
    fig, axs = plt.subplots(
            SIZE, SIZE, sharex=True, sharey=True,
            squeeze=False, figsize=(7, 2.5))

    for i, ax in enumerate(axs.flatten()):
        fr = solutions[i].flow_rate
        ax.plot(it, fr, '.--', color='black', drawstyle='steps-post', alpha=0.2)
        for j, (s, e) in enumerate(solutions[i].get_parts_flow_rate()):
            style = 'o-' if s == e else '.-'
            ax.plot(it[s:e+1], fr[s:e+1], '.-', drawstyle='steps-post', color=colors[j])
            ax.plot((it[e], it[e]+1), (fr[e], fr[e]), color=colors[j])
        ax.grid()
        ax.set_xlabel('Heures (h)')
        ax.set_ylabel('Débit massique (kg/s)')
        ax.set_xlim(min(it), max(it))
        ax.set_ylim(-p.dot_m_max, p.dot_m_max)

    if filename is not None:
        plt.savefig(FIGBASE + filename)
    plt.show(block=False)


# Initialize the population
chp = LinearCHP()
mes = MESMetaheuristic(dt=dt, chp=chp)
solver = GASolver(mes, size=SIZE**2)
pop = solver.ga.population

# show(pop, 'parts_flow_rate.pdf')

# for _ in range(1):
    # pop = [sol.mutate() for sol in pop]
    # show(pop)

pop = [pop[0].mutation_random()]
show(pop, 'mutation_random_change.pdf')

fig = plt.figure()
plt.plot(dt, [p.T[t] for t in dt])
# plt.show(block=True)

input('Press enter to exit')
