import numpy as np
import matplotlib.pyplot as plt

from src.mes import MES
from src.chp import LinearCHP
from src.solver import Solver

FIGBASE = '/home/antoine/ownCloud/1 Projects/Doctorat/5 Documents/2023-04 Thèse/Manuscrit/Figures/epsilon_example_'
fignum = 1

chp = LinearCHP()
dt = list(range(1080, 1080 + 168))
mes = MES(dt=dt, chp=chp)
solver = Solver(mes)
solver.init_variables()
solver.set_constraints()

results = []

first = solver.optimize()
results.append(first)

last = solver.optimize(prioritizeRES=True)
results.append(last)

# TODO: Generate first figure

x = [first.ACR, last.ACR]
y = [first.RES, last.RES]
plt.figure(figsize=(4, 3))
ax = plt.subplot(111)
ax.plot(x, y, 'o')
for vi, xy in enumerate(zip(x, y)):
    ax.annotate(f'({vi+1})', xy=xy, xycoords='data', xytext=(4, 4), textcoords='offset points')
ax.set_xlabel(r'$ATCR$')
ax.set_ylabel(r'$\tau_{EnR}$')
ax.grid(True)
plt.savefig(FIGBASE + str(fignum) + '.pdf')
fignum += 1
plt.show()

xlim = ax.get_xlim()
ylim = ax.get_ylim()

samples = np.linspace(first.RES, last.RES, 4)
samples = samples[1:-1]

# TODO: Plot with epsilon

# x = [first.ACR, last.ACR]
# y = [first.RES, last.RES]
# ax = plt.subplot(111)
# ax.axhline(y=samples[0], color='gray', alpha=0.7, linestyle='--')
# ax.fill_between(
    # (0, 1), (samples[0], samples[0]), hatch='//', color='gray', fc='none',
    # alpha=0.7, transform=ax.get_yaxis_transform())
# ax.plot(x, y, 'o')
# ax.set_xlim(xlim)
# ax.set_ylim(ylim)
# ax.set_xlabel(r'$ATCR$')
# ax.set_ylabel(r'$\tau_{EnR}$')
# ax.grid(True)
# plt.show()

for i, epsilon in enumerate(samples):
    result = solver.optimize(epsilon=epsilon)
    results.append(result)

    # TODO: Plot intermediate front

    x = [r.ACR for r in results]
    y = [r.RES for r in results]
    plt.figure(figsize=(4, 3))
    ax = plt.subplot(111)
    ax.axhline(y=epsilon, color='gray', alpha=0.7, linestyle='--')
    ax.fill_between(
        (0, 1), (epsilon, epsilon), hatch='/', color='gray', fc='none',
        alpha=0.7, transform=ax.get_yaxis_transform())
    ax.plot(x, y, 'o')
    for vi, xy in enumerate(zip(x, y)):
        ax.annotate(f'({vi+1})', xy=xy, xycoords='data', xytext=(4, 4), textcoords='offset points')
    # ax.plot(last.ACR, last.RES, 'o', color='C0')
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xlabel(r'$ATCR$')
    ax.set_ylabel(r'$\tau_{EnR}$')
    ax.grid(True)
    plt.savefig(FIGBASE + str(fignum) + '.pdf')
    fignum += 1
    plt.show()

# TODO: Plot final result

results.sort(key=lambda r: r.ACR)

print([(r.ACR, r.RES) for r in results])

x = [r.ACR for r in results]
y = [r.RES for r in results]
plt.figure(figsize=(4, 3))
ax = plt.subplot(111)
ax.plot(x, y, 'o--')
ax.set_xlim(xlim)
ax.set_ylim(ylim)
ax.set_xlabel(r'$ATCR$')
ax.set_ylabel(r'$\tau_{EnR}$')
ax.grid(True)
plt.savefig(FIGBASE + str(fignum) + '.pdf')
fignum += 1
plt.show()
