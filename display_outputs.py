import argparse
import tempfile
import os
from os.path import splitext
from zipfile import ZipFile
import matplotlib.pyplot as plt
import json
import numpy as np

from parameters import (L_e, n_EB, L_h)
from mp import Result, P_h_ST_max
from utils import sorted_alphanumeric


def load_arguments(zipfile):
    zipfile.extract('arguments.json')
    with open('arguments.json', 'r') as f_in:
        args = json.load(f_in)
    return args


def ask_choice(choices):
    for i, c in enumerate(choices):
        print(' {})\t{}'.format(i+1, c))
    index = int(input('Choice? '))
    try:
        choice = choices[index-1]
    except IndexError:
        raise Exception('Incorrect choice')
    print(choice)
    return choice


def plot_elec(r, ax):
    idx = list(range(len(r.operation.index)))
    # idx = np.argsort(dL_h)[::-1]
    idt = np.array(r.operation.index)[idx]

    dL_e = np.array(L_e)[r.operation.index]
    P_e_CHP = r.operation['P_e_CHP'][idt]
    P_e_PV = r.operation['P_e_PV'][idt]
    U_e = r.operation['U_e'][idt]
    V_e = r.operation['V_e'][idt]
    P_h_EB = r.operation['P_h_EB'][idt]
    F_e_EB = [P/n_EB for P in P_h_EB]

    ax.stackplot(idt, P_e_CHP, P_e_PV, U_e, V_e,
                 labels=['CHP', 'PV (consumed)', 'Grid', 'PV (sold)'])
    ax.plot(idt, dL_e[idx], color='black', linestyle='-', label='Electricity demand')
    ax.plot(idt, dL_e[idx] + F_e_EB, color='black', linestyle='--', label='EB demand')
    plt.legend()
    # plt.legend(loc='upper right')
    plt.xlim(min(idt), max(idt))
    plt.xlabel('Hours [h]')
    plt.ylabel('Electrical energy [kW]')


def plot_heat(r, ax):
    idx = list(range(len(r.operation.index)))
    # idx = np.argsort(dL_h)[::-1]
    idt = np.array(r.operation.index)[idx]

    dL_h = np.array(L_h)[r.operation.index]
    A_ST = r.dimensioning['A_ST'][0]
    P_h_CHP = r.operation['P_h_CHP'][idt]
    P_h_GB = r.operation['P_h_GB'][idt]
    P_h_EB = r.operation['P_h_EB'][idt]
    P_h_ST = r.operation['P_h_ST'][idt]
    P_h_ST_unused = [P_h_ST_max(t, A_ST) - P_h_ST[t] for t in idt]

    # plt.setp(ax2.get_xticklabels(), visible=False)
    plt.stackplot(idt, P_h_CHP, P_h_GB, P_h_EB, P_h_ST, P_h_ST_unused,
                  labels=['CHP', 'GB', 'EB', 'ST (consumed)', 'ST (not used)'])
    plt.plot(idt, dL_h[idx], color='black', linestyle='-', label='Heat demand')
    plt.legend()
    # plt.legend(loc='upper right')
    # plt.xlim(min(idx), max(idx))
    plt.xlabel('Hours [h]')
    plt.ylabel('Heat [kW]')


parser = argparse.ArgumentParser(description='Display outputs times series')
parser.add_argument(
    'archives', metavar='archive', type=str, nargs='+', help='archive of the results')
parser.add_argument('--out', type=str, help='output file')

args = parser.parse_args()

fig = plt.figure(figsize=[6.4*2, 4.8*len(args.archives)])

sorted_archives = sorted_alphanumeric(args.archives)

for i, archive in enumerate(sorted_archives):
    print(archive)
    with ZipFile(archive) as myzip:
        files = [n for n in myzip.namelist() if n.endswith('.log')]
        files = sorted_alphanumeric(files)
        files = [splitext(f)[0] for f in files]
        file_choice = ask_choice(files)
        with tempfile.TemporaryDirectory() as tmpdir:
            wdir = os.getcwd()
            os.chdir(tmpdir)
            arguments = load_arguments(myzip)

            to_extract = ['{}-objectives.csv'.format(file_choice)]
            to_extract.append('{}-operation.csv'.format(file_choice))
            to_extract.append('{}-dimensioning.csv'.format(file_choice))
            myzip.extractall(members=to_extract)
            r = Result.load_from_file(
                file_choice, sheets=['objectives', 'operation', 'dimensioning'])

            # t1 = arguments['start']
            # t2 = t1 + arguments['duration']

            axs = fig.add_subplot(len(args.archives), 2, 2*i + 1)
            plot_elec(r, axs)
            # plt.plot(range(t1, t2), L_e[t1:t2], '--', label='L_e')
            # r.operation[['P_e_CHP', 'P_e_PV', 'U_e', 'V_e']].plot(ax=axs)
            # plt.xlim(t1, t2-1)
            # plt.legend()

            axs = fig.add_subplot(len(args.archives), 2, 2*i + 2, sharex=axs)
            plot_heat(r, axs)
            # plt.plot(range(t1, t2), L_h[t1:t2], '--', label='L_h')
            # r.operation[['P_h_CHP', 'P_h_GB', 'P_h_EB', 'P_h_ST']].plot(ax=axs)
            # plt.xlim(t1, t2-1)
            # plt.legend()

            # plt.show()

            # import ipdb; ipdb.set_trace()

            os.chdir(wdir)

if args.out is None:
    plt.show()
else:
    plt.savefig(args.out, bbox_inches='tight', dpi=200)
plt.close()
