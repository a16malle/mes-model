import datetime
import os

from src.mes import MESWithMixedTES, LinearCHP
from src.results import save_result


DIRNAME = 'tests'
START = 1080
DURATION = 168


def get_save_path(S_TES):
    """Return the path name for this experiment."""
    head = DIRNAME
    date = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
    suffix = '{}+{}-S_TES{}-{}'.format(START, DURATION, S_TES, date)
    path = os.path.join(head, suffix)
    return path


chp = LinearCHP
chp_args = {}


for S in (100, 500, 1000, 2000, 3000, 5000, 10000):
    log_file = get_save_path(S) + '_gurobi.log'
    filename = get_save_path(S) + '.pkl.gz'
    mes = MESWithMixedTES(list(range(START, START + DURATION)), chp=chp, chp_args=chp_args, log_file=log_file, S_TES=S)
    mes.set_constraints()
    mes.init_objectives()
    result = mes.optimize(prioritizeRES=True)
    result.display()
    save_result(filename, result, {'start': START, 'duration': DURATION, 'S_TES': S, 'dir': DIRNAME, 'linearization': None, 'tes': 'mixed'})
