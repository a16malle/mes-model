# flake8: noqa
import argparse
import os

from src.results import load_result, save_result
from src.mes import AdaptedCHP, MES, MESApproximate
from src.utils import bcolors as ts, get_difference, get_save_path, show_comparison


EPS_GAP = 1e-2

def solve_optimal(start, duration, N, number, show_log):
    filename = get_save_path(start, duration, N, number, prefix='optimal-', directory=args.directory)
    if os.path.exists(filename) and not args.overwrite:
        print('Loading from file "{}"'.format(filename))
        results = load_result(filename)
        return results
    chp = AdaptedCHP
    chp_args = {'N': N}
    dt = list(range(start, start + duration))
    mes = MES(dt=dt, chp=chp, chp_args=chp_args, output_screen=show_log, epsilon_gap=EPS_GAP)
    mes.set_constraints()
    mes.init_objectives()
    results = mes.optimize_pareto(number)
    results.arguments = {
        'start': start,
        'duration': duration,
        'N': N
    }
    save_result(filename, results)
    return results


def solve_approximate(start, duration, N, number, show_log, directory=''):
    filename = get_save_path(start, duration, N, number, prefix='approximate-', directory=args.directory)
    if os.path.exists(filename) and not args.overwrite:
        print('Loading from file "{}"'.format(filename))
        results = load_result(filename)
        return results
    chp = AdaptedCHP
    chp_args = {'N': N}
    dt = list(range(start, start + duration))
    mes = MESApproximate(dt=dt, chp=chp, chp_args=chp_args, output_screen=show_log, epsilon_gap=EPS_GAP)
    mes.set_constraints()
    mes.init_objectives()
    results = mes.optimize_pareto(number)
    results.arguments = {
        'start': start,
        'duration': duration,
        'N': N
    }
    save_result(filename, results)
    return results


def run_models(start, duration, N, number, show_log=False):
    print(f'{ts.HEADER}###### Optimal resolution ######{ts.ENDC}')
    print('')
    optimal = solve_optimal(start, duration, N, number, show_log)

    print(f'{ts.HEADER}###### Approximate resolution ######{ts.ENDC}')
    print('')
    heuristic = solve_approximate(start, duration, N, number, show_log)
    return optimal, heuristic


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute and plot results')
    parser.add_argument('directory', type=str, help='directory to save the results')
    parser.add_argument('-o', '--overwrite', help='overwrite the results files', action='store_true')
    parser.add_argument('-s', '--start', type=int, nargs='+', default=[1080, 3936, 6228], help='start time (default: 0)')
    parser.add_argument('-d', '--duration', type=int, default=336, help='duration in hours (default: 168, i.e. one week)')
    parser.add_argument('-N', type=int, default=10, help='number of triangles (default: 10)')
    parser.add_argument('-n', '--number', type=int, default=10, help='number of Pareto solution (default: 10)')
    args = parser.parse_args()

    kwargs = {
            'show_log': True,
            'duration': args.duration,
            'N': args.N,
            'number': args.number,
    }

    for i, start in enumerate(args.start):
        print(f'{ts.HEADER}######### Start {start}+{kwargs["duration"]} ({i+1}/{len(args.start)}) #########{ts.ENDC}')
        optimal, heuristic = run_models(start=start, **kwargs)
        show_comparison(optimal, heuristic, plot=True, show_comparison=True)
