# flake8: noqa

"""Plot the non-linear power function."""

from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

from src.parameters import a, b, c

x = np.linspace(100, 1000, 100)  # P_nom
ratio = np.linspace(0, 1, 100)  # ratio
x, ratio = np.meshgrid(x, ratio)
y = x*ratio

n_e = a + b*ratio + c*ratio**2  # eta_e

# n_e[x < 100] = None
# n_e[y > x] = None
F_g = y/n_e
z = 0.8*(F_g - y)  # P_h

ax = plt.figure().add_subplot(projection='3d')
ax.plot_surface(x, y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.set(xlim=(0, 1000), ylim=(0, 1000), xlabel='$P_{CHP,nom}$', ylabel='$P_{e,CHP,t}$', zlabel='$P_{h,CHP,t}$')
plt.show()
