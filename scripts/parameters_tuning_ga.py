from itertools import product
from collections import namedtuple
import pandas as pd
import logging
import logging.handlers
import sys
import os
import os.path
import time
import json
import argparse
import shutil
import gzip
import multiprocessing as mp

from src.solver import GASolver, GAHeuristicSolver
from src.mes import MESMetaheuristic
from src.chp import LinearCHP, AdaptedCHP
from src.utils import pbar_manager


logger = logging.getLogger(__name__)


LOG_SIZE = 20*1024*1024  # Max size of 20MB before compressing
HEURISTIC = True


def namer(name):
    return name + '.gzip'


def rotator(source, dest):
    with open(source, 'rb') as f_in:
        with gzip.open(dest, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove(source)


class redirect_stdout():
    def __init__(self, lock=None):
        self.lock = lock

    def __enter__(self):
        if self.lock is not None:
            self.lock.acquire()
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = sys.__stdout__
        if self.lock is not None:
            self.lock.release()


def set_logger(filename, stream=True):
    base_logger = logging.getLogger('')
    base_logger.setLevel(logging.INFO)
    formatter = logging.Formatter(
        fmt='%(asctime)s:%(levelname)s:%(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S')
    if filename is not None:
        file_handler = logging.handlers.RotatingFileHandler(filename, maxBytes=LOG_SIZE, backupCount=1000)
        file_handler.rotator = rotator
        file_handler.namer = namer
        file_handler.setFormatter(formatter)
        base_logger.addHandler(file_handler)
    if stream:
        stream_handler = logging.StreamHandler(sys.__stdout__)
        stream_handler.setFormatter(formatter)
        base_logger.addHandler(stream_handler)


# FORMAT = '%(message)s'
# logging.basicConfig(level=logging.INFO, format=FORMAT)
# logging.getLogger('src.solver.heuristic').addFilter(lambda record: False)
# logging.getLogger('gurobipy.gurobipy').addFilter(lambda record: False)


Parameters = namedtuple('Parameters', [
    'size', 'nbr_iterations', 'nbr_no_improvement',
    'crossover_best_weight', 'sigma_factor', 'elite_percent'])


class ParameterSet:
    def __init__(self, filename):
        self.basename, ext = os.path.splitext(filename)
        with open(filename, 'r') as f:
            data = json.load(f)
            self.nodes = data['nodes']
            self.start = data['start']
            self.duration = data['duration']
            self.nbr_runs = data['nbr_runs']
            self.size = data['size']
            self.nbr_iterations = data['nbr_iterations']
            self.nbr_no_improvement = data['nbr_no_improvement']
            self.crossover_best_weight = data['crossover_best_weight']
            self.sigma_factor = data['sigma_factor']
            self.elite_percent = data['elite_percent']

    def combinations(self):
        combinations = product(
            self.size, self.nbr_iterations, self.nbr_no_improvement,
            self.crossover_best_weight, self.sigma_factor, self.elite_percent)
        for c in combinations:
            yield Parameters(*c)


class Results:
    def __init__(self):
        self.df = pd.DataFrame({
            'size': pd.Series(dtype='int'),
            'nbr_iterations': pd.Series(dtype='int'),
            'nbr_no_improvement': pd.Series(dtype='int'),
            'crossover_best_weight': pd.Series(dtype='float'),
            'sigma_factor': pd.Series(dtype='float'),
            'elite_percent': pd.Series(dtype='float'),
            'duration': pd.Series(dtype='float'),
            'fitness': pd.Series(dtype='float'),
            'seed': pd.Series(dtype='float')})

    def save(self, filename):
        """Save DataFrame to a file."""
        self.df.to_json(filename)

    def load(self, filename):
        """Load the results to a DataFrame."""
        dtype = {
            'size': 'int',
            'nr_iterations': 'int',
            'nbr_no_improvement': 'int',
            'crossover_best_weight': 'float',
            'sigma_factor': 'float',
            'elite_percent': 'float',
            'duration': 'float',
            'fitness': 'float',
            'seed': 'float'
        }
        self.df = pd.read_json(filename, dtype=dtype, precise_float=True)

    def append_result(self, params, duration, fitness, seed):
        """Add a new result to the DataFrame."""
        new_df = pd.DataFrame({
            'size': pd.Series([params.size], dtype='int'),
            'nbr_iterations': pd.Series([params.nbr_iterations], dtype='int'),
            'nbr_no_improvement': pd.Series([params.nbr_no_improvement], dtype='int'),
            'crossover_best_weight': pd.Series([params.crossover_best_weight], dtype='float'),
            'sigma_factor': pd.Series([params.sigma_factor], dtype='float'),
            'elite_percent': pd.Series([params.elite_percent], dtype='float'),
            'duration': pd.Series([duration], dtype='float'),
            'fitness': pd.Series([fitness], dtype='float'),
            'seed': pd.Series([seed], dtype='float')
        })
        self.df = pd.concat([self.df, new_df], ignore_index=True)

    def exists(self, run_nbr, params):
        # keys = ['size', 'nbr_iterations', 'nbr_no_improvement',
                # 'crossover_best_weight', 'sigma_factor', 'elite_percent']
        # grouped = self.df.groupby(keys).agg(
                # count=('seed', 'count'))
        comp_df = pd.DataFrame({
            'size': pd.Series([params.size], dtype='int'),
            'nbr_iterations': pd.Series([params.nbr_iterations], dtype='int'),
            'nbr_no_improvement': pd.Series([params.nbr_no_improvement], dtype='int'),
            'crossover_best_weight': pd.Series([params.crossover_best_weight], dtype='float'),
            'sigma_factor': pd.Series([params.sigma_factor], dtype='float'),
            'elite_percent': pd.Series([params.elite_percent], dtype='float'),
        })
        filtered = pd.merge(self.df, comp_df, on=list(comp_df.columns))
        return len(filtered) > run_nbr

    def __str__(self):
        keys = ['size', 'nbr_iterations', 'nbr_no_improvement',
                'crossover_best_weight', 'sigma_factor', 'elite_percent']
        grouped = self.df.groupby(keys).agg(
                duration=('duration', 'mean'),
                fitness=('fitness', 'mean'),
                count=('seed', 'count')).sort_values(by='fitness')
        return grouped.to_string()


def run(params, dt, nodes=1, seed=None, lock=None):
    if HEURISTIC:
        chp = AdaptedCHP(10)
    else:
        chp = LinearCHP()
    with redirect_stdout(lock):
        mes = MESMetaheuristic(dt=dt, chp=chp)
    crossover_weights = (params.crossover_best_weight,
                         1 - params.crossover_best_weight)
    if HEURISTIC:
        solver_cls = GAHeuristicSolver
    else:
        solver_cls = GASolver
    solver = solver_cls(
        mes, epsilon_gap=1e-2, nodes=nodes, size=params.size,
        nbr_iterations=params.nbr_iterations,
        nbr_no_improvement=params.nbr_no_improvement, crossover_weights=crossover_weights,
        sigma_factor=params.sigma_factor, elite_percent=params.elite_percent, seed=seed)
    with redirect_stdout(lock):
        solver.init_variables()
        solver.set_constraints()
        result = solver.optimize(prioritizeRES=True)
    sys.stdout = sys.__stdout__
    # result.display()
    # logger.info(f'Storage: {result.storage}')
    return solver.ga.get_ordered()[0], result


def start(param_set, result_file):
    results = Results()
    if os.path.exists(result_file):
        results.load(result_file)
    combinations = list(param_set.combinations())
    dt = list(range(param_set.start, param_set.start + param_set.duration))
    with pbar_manager('Parameter tuning') as manager:
        pbar_run = manager.counter(total=param_set.nbr_runs, desc='Runs')
        for i in pbar_run(range(param_set.nbr_runs)):
            pbar_param = manager.counter(
                total=len(combinations), desc='Parameter set', leave=False)
            for c in pbar_param(combinations):
                logger.info(f'Parameters: {c._asdict()}')
                if results.exists(i, c):
                    logger.info('Already in result file')
                    continue
                results.exists(i, c)
                seed = time.time()
                best, result = run(c, dt, nodes=param_set.nodes, seed=seed)
                result.display()
                duration = result.duration
                fitness = best.fitness
                results.append_result(c, duration, fitness, seed)
                results.save(result_file)
                logger.info('Results:\n' + str(results))
            pbar_param.close()
            # input('Waiting for key press...')
        logger.info(f'Total duration: {pbar_run.elapsed}s')
        pbar_run.close()


def start_process(lock, param_set, queue, results, result_file):
    process_id = os.getpid()
    log_file = f'{param_set.basename}.{process_id}.log'
    set_logger(log_file, stream=False)
    dt = list(range(param_set.start, param_set.start + param_set.duration))
    while not queue.empty():
        iteration, combination = queue.get()
        print(f'Iter. {iteration}: {combination}')
        logger.info(f'Iter. {iteration}: {combination}')
        if results.exists(iteration, combination):
            logger.info('Already exists in result file')
            continue
        seed = process_id + time.time()
        best, result = run(combination, dt, nodes=param_set.nodes, seed=seed)
        duration = result.duration
        fitness = best.fitness

        # Lock results to not change from multiple processes
        with lock:
            if os.path.exists(result_file):
                results.load(result_file)
            results.append_result(combination, duration, fitness, seed)
            results.save(result_file)

        logger.info('Results:\n' + str(results))
        time.sleep(0.1)
    logger.info(f'Queue empty in process {process_id}')


def start_processes(args):
    lock = mp.Lock()
    # Load the parameters
    param_set = ParameterSet(args.file)
    queue = mp.JoinableQueue()
    for nbr in range(param_set.nbr_runs):
        for c in param_set.combinations():
            queue.put((nbr, c))

    # Load the results
    results = Results()
    result_file = f'{param_set.basename}.out.json.gzip'
    if os.path.exists(result_file):
        results.load(result_file)

    # Start processes
    pool = []
    for i in range(args.process):
        p = mp.Process(target=start_process, args=(lock, param_set, queue, results, result_file))
        pool.append(p)

    with pbar_manager('Parameter tuning multi-process', lock=True) as manager:
        total = queue.qsize()
        counter = manager.counter(total=total, desc='Runs')

        for p in pool:
            p.start()

        previous = 0
        while any(p.is_alive() for p in pool):
            count = total - queue.qsize()
            counter.count = count
            counter.refresh()
            if count != previous and os.path.exists(result_file):
                with lock:
                    results.load(result_file)
                    print(results)
                previous = count
            time.sleep(0.5)
        counter.count = total - queue.qsize()
        counter.refresh()
        counter.close()

    # mp.connection.wait(p.sentinel for p in pool)
    print('Processes finished')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Start hyper-parameters tuning')
    parser.add_argument('file', type=str, help='File with the parameters set')
    parser.add_argument('-p', '--process', type=int, default=1, help='Number of processes to start')
    args = parser.parse_args()

    if args.process > 1:
        start_processes(args)
    else:
        param_set = ParameterSet(args.file)
        result_file = f'{param_set.basename}.out.json.gzip'
        log_file = f'{param_set.basename}.log'
        set_logger(log_file)
        start(param_set, result_file)
