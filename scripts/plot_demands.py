# flake: noqa

"""Plot the heat and electricity demands for a given period."""

from src import parameters as p
import matplotlib.pyplot as plt

dt = list(range(1080, 1080+168))
L_e = [p.L_e[t] for t in dt]
L_h = [p.L_h[t] for t in dt]
ax = plt.subplot(111)
ax.plot(dt, L_e, color='blue', label='Electricity')
ax.plot(dt, L_h, color='red', label='Heat')
ax.set_xlabel('Hours (h)')
ax.set_ylabel('Energy demand (kW)')
ax.legend()
ax.grid(True)
ax.set_xlim(min(dt), max(dt))
ax.set_ylim(bottom=0)
plt.show()
