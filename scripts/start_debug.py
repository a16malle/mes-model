# flake8: noqa
import matplotlib.pyplot as plt
import logging

from src.solver import Solver, GASolver, GAHeuristicSolver, HeuristicSolver
from src.mes import MES, MESWithGabrielliTES, MESWithMixedTES, MESMetaheuristic
from src.chp import LinearCHP, AdaptedCHP

FORMAT = '%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logging.getLogger('src.solver.heuristic').addFilter(lambda record: False)
logging.getLogger('gurobipy.gurobipy').addFilter(lambda record: False)


# START = 0
START = 1080
# START = 3936
# START = 6228
DURATION = 72
# DURATION = 168
# DURATION = 336
# DURATION = 672
# DURATION = 4380
# DURATION = 8760


def solve_without_TES(solver_cls, mes_cls=MES, chp=None, duration=168, MIPGap=None, **solver_args):
    if chp is None:
        chp = LinearCHP()
    # chp = AdaptedCHP(N=10)
    # dt = list(range(1080, 1080+24))
    dt = list(range(START, START+duration))
    # dt = list(range(1080, 1080+336))
    # dt = list(range(0, 8760))
    mes = mes_cls(dt=dt, chp=chp)
    if MIPGap is not None:
        mes.m.setParam('MIPGap', MIPGap)
    solver = solver_cls(mes, epsilon_gap=1e-2, **solver_args)
    solver.init_variables()
    solver.set_constraints()
    result = solver.optimize(prioritizeRES=True)
    result.plot(fig=plt.figure(0))
    return result

def solve(solver_cls, mes_cls=MES, chp=None, duration=168, MIPGap=None, radius=2.5, **solver_args):
    if chp is None:
        chp = LinearCHP()
    # chp = AdaptedCHP(N=10)
    # dt = list(range(1080, 1080+24))
    dt = list(range(START, START+duration))
    # dt = list(range(1080, 1080+336))
    # dt = list(range(0, 8760))
    mes = mes_cls(dt=dt, chp=chp, radius=radius)
    if MIPGap is not None:
        mes.m.setParam('MIPGap', MIPGap)
    solver = solver_cls(mes, epsilon_gap=1e-2, **solver_args)
    solver.init_variables()
    solver.set_constraints()
    result = solver.optimize(prioritizeRES=True)
    result.plot(fig=plt.figure(0))
    return result


def solve_ga(duration=168, solution=None, MIPGap=None, **solver_args):
    dt = list(range(START, START+duration))
    mes = MESMetaheuristic(dt=dt, chp=LinearCHP())
    solver = GASolver(mes, epsilon_gap=1e-2, **solver_args)
    # mes = MESMetaheuristic(dt=dt, chp=AdaptedCHP(N=5))
    # solver = GAHeuristicSolver(mes, epsilon_gap=1e-2, **solver_args)
    if MIPGap is not None:
        mes.m.setParam('MIPGap', MIPGap)
    if solution is not None:
        solver.add_solution(**solution)
    print('Creating model...')
    solver.init_variables()
    print('Adding constraints...')
    solver.set_constraints()
    try:
        print('Starting optimization...')
        result = solver.optimize(prioritizeRES=True)
    except KeyboardInterrupt:
        print('Interrupting')
    results = solver.get_results(2)
    for i, r in enumerate(results):
        r.display(design=True, operation=False)
        print(r.storage)
        fig = plt.figure(i+1)
        r.plot(fig=fig)


if __name__ == '__main__':
    # chp = LinearCHP()
    # r1 = solve(Solver, chp=chp, duration=duration, MIPGap=0.02)
    # chp = AdaptedCHPSOS(N=10)
    # r2 = solve(Solver, chp=chp, MIPGap=0.0001)
    # utils.print_energy_balance(r1, r2, legend=['Linearization', 'SOS'])
    # r1.display()
    # r2.display()
    # utils.plot_bisector(r1, r2, ['Linearization', 'SOS'])
    # plt.show()
    # utils.plot_result(r1, r2, legend=['Linearization', 'SOS'])
    # plt.show()
    # solve(GASolver, MIPGap=0.02, size=10)
    # chp = AdaptedCHP(N=10)
    # r3 = solve(HeuristicSolver, chp=chp, MIPGap=0)
    # utils.print_energy_balance(r2, r3, legend=['SOS', 'Heuristic'])
    # utils.plot_result(r1, legend=['Linearization', 'SOS', 'Heuristic'])
    # r4 = solve(GASolver, chp=LinearCHP(), MIPGap=0.02, size=20)

    opt = solve(Solver, MESWithMixedTES, duration=DURATION, MIPGap=0.05, radius=2.5)
    # opt = solve(Solver, MESWithMixedTES, duration=DURATION, MIPGap=0.1)

    # solution = {
            # # # 'temperatures': list(opt.storage['T_TES']),
            # 'F': list(opt.operation['F_h_TES']),
            # 'P': list(opt.operation['P_h_TES']),
            # 'r': 2.5,
            # }

    # solve_without_TES(HeuristicSolver, chp=AdaptedCHP(N=10), duration=DURATION)

    solve_ga(DURATION, size=30, nbr_iterations=100, nbr_no_improvement=None, solution=None, MIPGap=0, nodes=1)

    opt.display(design=True, operation=False)
    print(opt.storage)

    # plt.show()
