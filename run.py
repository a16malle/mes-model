import argparse
import os
from enum import Enum
import logging
import sys

from src.mes import MES, MESWithGabrielliTES, MESWithMixedTES
from src.chp import AdaptedCHP, LinearCHP, NonlinearCHP
from src.solver import Solver, HeuristicSolver
from src.results import save_result


logger = logging.getLogger(__name__)


def set_logger(filename):
    base_logger = logging.getLogger('')
    base_logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        fmt='%(asctime)s:%(levelname)s:%(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S')
    if filename is not None:
        file_handler = logging.FileHandler(filename)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(logging.DEBUG)
        base_logger.addHandler(file_handler)
    stream_handler = logging.StreamHandler(sys.__stdout__)
    stream_handler.setFormatter(formatter)
    base_logger.addHandler(stream_handler)


def log_args(args):
    logger.info('Command-line arguments:')
    for arg in vars(args):
        logger.info(f'  {arg}: {getattr(args, arg)}')


class CHP(Enum):
    linear = LinearCHP
    adapted = AdaptedCHP
    nonlinear = NonlinearCHP


class Resolution(Enum):
    exact = Solver
    heuristic = HeuristicSolver


class TES(Enum):
    mixed = 'mixed'
    gabrielli = 'gabrielli'


class ObjectiveRES(Enum):
    ACR = False
    RES = True


class EnumAction(argparse.Action):
    """
    Argparse action for handling Enums
    """
    def __init__(self, **kwargs):
        # Pop off the type value
        enum_type = kwargs.pop("type", None)

        # Ensure an Enum subclass is provided
        if enum_type is None:
            raise ValueError("type must be assigned an Enum when using EnumAction")
        if not issubclass(enum_type, Enum):
            raise TypeError("type must be an Enum when using EnumAction")

        # Generate choices from the Enum
        kwargs.setdefault("choices", tuple(e.name for e in enum_type))

        super(EnumAction, self).__init__(**kwargs)

        self._enum = enum_type

    def __call__(self, parser, namespace, values, option_string=None):
        # Convert value back into an Enum
        value = self._enum[values]
        setattr(namespace, self.dest, value)


def get_save_path(args):
    """Return the path name for this experiment."""
    head = args.dir
    suffix = '{}+{}_{}_{}_{}_{}_{}'.format(
        args.start, args.duration, args.linearization, args.N, args.M, args.tes, args.number)
    # import datetime
    # date = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
    # suffix = '{}+{}_{}_{}_{}_{}_{}_{}'.format(
        # args.start, args.duration, args.linearization, args.N,
        # args.M, args.tes, args.number, date)
    # suffix = '{}+{}-S_TES{}-{}'.format(args.start, args.duration, args.S_TES, date)
    path = os.path.join(head, suffix)
    return path


def start(args):
    # Parse the linearization method
    if args.linearization is CHP.adapted:
        chp_args = {'N': args.N}
    else:
        chp_args = {}
    chp = args.linearization.value(**chp_args)

    filename = get_save_path(args) + '.pkl.gz'
    dt = list(range(args.start, args.start + args.duration))
    if args.tes == TES.gabrielli:
        mes = MESWithGabrielliTES(dt=dt, chp=chp)
    elif args.tes == TES.mixed:
        mes = MESWithMixedTES(dt=dt, chp=chp, radius=args.radius)
    else:
        mes = MES(dt=dt, chp=chp)

    solver = args.solver.value(mes, epsilon_gap=1e-2, epsilons=args.epsilons)
    solver.init_variables()
    solver.set_constraints()

    # mes.m.setParam('MIPGap', 0.001)
    if args.mipgap > 0:
        mes.m.setParam('MIPGap', args.mipgap)
    if args.timelimit is not None:
        mes.m.setParam('TimeLimit', args.timelimit)
    if args.number > 1:
        results = solver.optimize_pareto(args.number)
        # results.arguments = vars(args)
        print('Total duration: {:.3f}s'.format(results.duration))
        if args.plot:
            results.plot_pareto()
        if args.dir is not None:
            save_result(filename, results)
    else:
        result = solver.optimize(prioritizeRES=args.objective.value)
        print('Duration: {:.3f}s'.format(result.duration))
        if args.plot:
            result.plot()
        if args.dir is not None:
            save_result(filename, result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Solve the mathematical program')
    parser.add_argument('--start', type=int, default=0, help='start time')
    parser.add_argument('--duration', type=int, default=8760, help='duration')
    parser.add_argument(
        '--objective', type=ObjectiveRES,
        default=ObjectiveRES.ACR, action=EnumAction,
        help=f'main objective function (default: {ObjectiveRES.ACR})')
    parser.add_argument('--number', type=int, default=1, help='number of solutions')
    parser.add_argument(
        '--linearization', type=CHP, default=CHP.linear, action=EnumAction,
        help=f'linearization method (default: {CHP.linear})')
    parser.add_argument('--N', type=int,
                        help='number of linearization break points')
    parser.add_argument('--M', type=int, help='other number of triangles')
    parser.add_argument('--tes', type=TES, help='type of TES', action=EnumAction)
    parser.add_argument(
        '--solver', type=Resolution, default=Resolution.exact, action=EnumAction,
        help=f'type of resolution (default: {Resolution.exact})')
    parser.add_argument('--radius', type=float, default=2.5,
                        help='radius of the mixed TES in kWh')
    parser.add_argument('--dir', type=str, default='out', help='output directory')
    parser.add_argument('--plot', '-P', help='plot result', action='store_true')
    parser.add_argument('--mipgap', type=float, default=0, help='MIP gap')
    parser.add_argument('--timelimit', type=int, default=None, help='time limit in seconds')
    parser.add_argument(
        '--epsilons', type=float, nargs='*', default=None, help='epsilons constraints')

    args = parser.parse_args()
    assert args.epsilons is None or len(args.epsilons) == args.number - 2

    with open(os.devnull, 'w') as sys.stdout:
        set_logger(get_save_path(args) + '.log')
        log_args(args)
        start(args)
