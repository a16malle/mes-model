#!/bin/bash

DIR="/home/antoine/ownCloud/1 Projects/Doctorat/5 Documents/2021-09 Article MES/article/img"

#python display_efficiencies.py --nodisplay server/results/1080-168/1080+168_other_10_*.zip --out "$DIR/efficiency-winter.pdf"
#python display_efficiencies.py --nodisplay server/results/3936-168/3936+168_other_10_*.zip --out "$DIR/efficiency-summer.pdf"
#python display_efficiencies.py --nodisplay server/results/6288-168/6288+168_other_10_*.zip --out "$DIR/efficiency-mid.pdf"

#python display_outputs.py server/results/1080-168/1080+168_other_10_*.zip --out "$DIR/outputs-winter-1.pdf"
#python display_outputs.py server/results/3936-168/3936+168_other_10_*.zip --out "$DIR/outputs-summer-1.pdf"
#python display_outputs.py server/results/6288-168/6288+168_other_10_*.zip --out "$DIR/outputs-mid-1.pdf"

python display_outputs.py server/results/1080-168/1080+168_other_10_*.zip --out "$DIR/outputs-winter-6.pdf"
python display_outputs.py server/results/3936-168/3936+168_other_10_*.zip --out "$DIR/outputs-summer-6.pdf"
python display_outputs.py server/results/6288-168/6288+168_other_10_*.zip --out "$DIR/outputs-mid-6.pdf"

python display_outputs.py server/results/1080-168/1080+168_other_10_*.zip --out "$DIR/outputs-winter-10.pdf"
python display_outputs.py server/results/3936-168/3936+168_other_10_*.zip --out "$DIR/outputs-summer-10.pdf"
python display_outputs.py server/results/6288-168/6288+168_other_10_*.zip --out "$DIR/outputs-mid-10.pdf"
