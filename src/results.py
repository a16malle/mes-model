import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import gzip
import pickle
from collections import namedtuple
import tempfile
from zipfile import ZipFile
import os
from os.path import splitext
import json
import re

from . import parameters as p
from .utils import (
    pretty_unit_energy, colors as cl, hatches as ht, colors_gray as cg,
    sorted_alphanumeric, translate as _
)


# TODO: Handle colors and hatching for figures


pd.set_option('display.float_format', lambda x: '%.3f' % x)


def load_result(filename):
    """Load all the results from a compressed pickle file."""
    if filename.endswith('.pkl.gz'):
        with gzip.open(filename, 'rb') as f:
            data = pickle.load(f)
    elif filename.endswith('.zip'):
        data = ResultGroup.load_from_zip(filename)
    else:
        raise Exception(f'Results file type not recognized: {filename}')
    return data


def save_result(filename, data):
    """Save the result to a compressed pickle file."""
    with gzip.open(filename, 'wb') as f:
        pickle.dump(data, f)


Run = namedtuple('Run', ['Status', 'Runtime'], defaults=[None, None])


class Result:
    def __init__(self, objectives, design, operation, run=Run(), duration=None, **kwargs):
        self.objectives = objectives
        self.design = design
        self.operation = operation
        self.dt = list(operation.index)
        self.run = run
        self.duration = duration
        self.kwargs = kwargs

    @classmethod
    def load(cls, filename):
        with gzip.open(filename, 'rb') as f:
            data = pickle.load(f)
            return data

    def save(self, filename):
        """Save the result to a compressed pickle file."""
        with gzip.open(filename, 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def load_from_csv(cls, basename):
        """To be able to read old CSV format."""
        try:
            objectives = pd.read_csv(basename+'-objectives.csv', index_col=0)
        except FileNotFoundError:
            objectives = pd.DataFrame()
        try:
            design = pd.read_csv(basename+'-dimensioning.csv', index_col=0)
        except FileNotFoundError:
            design = pd.DataFrame()
        try:
            operation = pd.read_csv(basename+'-operation.csv', index_col=0)
        except FileNotFoundError:
            operation = pd.DataFrame()
        try:
            with open(basename + '.log', 'r') as f:
                s = f.read()
                ex = re.findall('([0-9.]*?) seconds', s)
                vals = [float(v) for v in ex]
                duration = max(vals)
        except FileNotFoundError:
            duration = None
        return cls(objectives, design, operation, duration=duration)

    def _get_fixed_reference_cost(self):
        dt_ratio = len(self.dt)/8760
        max_L_h = max([p.L_h[t] for t in self.dt])

        C_gr_ref = sum([p.C_gr[t]*p.L_e[t] for t in self.dt])
        C_g_ref = p.C_g*sum([p.L_h[t]/p.n_GB for t in self.dt])

        C_o_ref_old = p.C_o_GB_v*sum(p.L_h)
        C_inv_ref_old = p.C_inv_GB*max(p.L_h)

        C_o_ref_new = p.C_o_GB_f*max_L_h*dt_ratio
        C_inv_ref_new = p.C_inv_GB*max_L_h

        ATC_ref_old = C_o_ref_old + C_gr_ref + C_g_ref + p.crf*C_inv_ref_old*dt_ratio
        ATC_ref_new = C_o_ref_new + C_gr_ref + C_g_ref + p.crf*C_inv_ref_new*dt_ratio

        ATC_sys = ATC_ref_old*(1 - self.objectives['ACR'][0]/100)
        return 100*(1 - ATC_sys/ATC_ref_new)

    @property
    def ACR(self):
        if self.kwargs.get('C_ref_fixed') is True:
            return self.objectives['ACR'][0]
        else:
            return self._get_fixed_reference_cost()

    @property
    def RES(self):
        P_RES = self.operation['P_e_PV'].sum() + self.operation['P_h_ST'].sum()
        P_cons = sum(p.L_e[t] + p.L_h[t] for t in self.dt)
        return 100*P_RES/P_cons
        # try:
            # return self.objectives['RES'][0]
        # except KeyError:
            # # For compatibility with old files
            # return self.objectives['EnR'][0]

    def display(self, objectives=True, design=True, operation=True):
        """Display a summary of the result."""
        def formatter(v): return f'{v:.3f}'
        if objectives:
            print('Objectives:')
            text = self.objectives.to_string(index=False, float_format=formatter)
            print(text)
        if design:
            print('Design variables:')
            text = self.design.to_string(index=False, float_format=formatter)
            print(text)
        if operation:
            print('Operation variables:')
            text = self.operation.describe().to_string(index=True, float_format=formatter)
            print(text)

    def print_energy_balance(self):
        heat = {
            'CHP': sum(self.operation['P_h_CHP']),
            'GB': sum(self.operation['P_h_GB']),
            'EB': sum(self.operation['P_h_EB']),
            'ST': sum(self.operation['P_h_ST']),
        }
        elec = {
            'CHP': sum(self.operation['P_e_CHP']),
            'PV': sum(self.operation['P_e_PV']),
            'U': sum(self.operation['U_e']),
        }
        line1 = '|'
        line2 = '|'
        for d in (heat, elec):
            for k, v in d.items():
                vt = ' '.join(pretty_unit_energy(v))
                s = max(len(k), len(vt))
                line1 += f' {k:^{s}} '
                line2 += f' {vt:^{s}} '
            line1 += '|'
            line2 += '|'
        print(line1)
        print(line2)

    def P_h_ST_max(self, A_ST, t):
        """Return the total energy producted by ST."""
        coeff = p.G[t]*p.n_ST - p.U_loss*(p.T_mean - p.T[t])
        if coeff < 0:
            coeff = 0
        return A_ST*coeff

    def P_e_PV_max(self, A_PV, t):
        """Return the total energy produced by PV."""
        T_cell = 30 + 0.0175*(p.G[t] - 300) + 1.14*(p.T[t] - 25)
        n_PV = p.n_ref*(1 - p.beta*(T_cell - p.T_ref))
        P_PV = A_PV*p.n_dc_ac*n_PV*p.G[t]
        return P_PV

    def _plot_environment(self, ax, hide_xlabel=False, colors=True):
        """Plot the environmental variables."""
        idx = list(range(len(self.dt)))
        idt = np.array(self.dt)[idx]

        ax.plot(idx, [p.T[t] for t in idt], color='blue')
        ax.set_ylabel('Temperature [°C]', color='blue')
        ax2 = ax.twinx()
        ax2.plot(idx, [p.G[t] for t in idt], color='red')
        ax2.set_ylabel('Solar global radiation [kW/m$^2$]', color='red')
        if not hide_xlabel:
            ax.set_xlabel('Hours [h]')

    def _plot_heat(self, ax, hide_xlabel=False, monotone=False, colors=True, legend=True):
        """Plot the heat balance."""
        dL_h = np.array(p.L_h)[self.dt]
        ix = list(range(len(self.dt)))
        if monotone:
            idx = np.argsort(dL_h)[::-1]
        else:
            idx = ix
        idt = np.array(self.dt)[idx]
        A_ST = self.design['A_ST'][0]
        P_h_CHP = self.operation['P_h_CHP'][idt]
        P_h_GB = self.operation['P_h_GB'][idt]
        P_h_EB = self.operation['P_h_EB'][idt]
        P_h_ST = self.operation['P_h_ST'][idt]
        P_h_ST_unused = [self.P_h_ST_max(A_ST, t) - P_h_ST[t] for t in idt]

        # plt.setp(ax1.get_xticklabels(), visible=False)
        if colors:
            clrs = [cl.CHP, cl.GB, cl.EB, cl.ST, cl.ST2]
        else:
            clrs = [cg.CHP, cg.GB, cg.EB, cg.ST, cg.ST2]
        stacks = ax.stackplot(
            ix, P_h_CHP, P_h_GB, P_h_EB, P_h_ST, P_h_ST_unused,
            labels=['CHP', 'GB', 'EB', _('ST (consumed)'), _('ST (not used)')],
            colors=clrs, lw=0.2, ls='-', edgecolor=(0, 0, 0, 0.3))
        if not colors:
            hatches = [ht.CHP, ht.GB, ht.EB, ht.ST, ht.ST2]
            for stack, hatch in zip(stacks, hatches):
                stack.set(hatch=hatch)
        ax.plot(ix, dL_h[idx], color='black', linestyle='-', label=_('Heat demand'))
        if legend:
            # ax.legend(
                # loc='upper center', bbox_to_anchor=(0.5, -0.3),
                # ncol=3, fancybox=True, fontsize='small'
            # )
            ax.legend(fontsize='small')
        ax.set_xlim(min(ix), max(ix))
        if not hide_xlabel:
            ax.set_xlabel(_('Hours (h)'))
        ax.set_ylabel(_('Heat (kW)'))

    def _plot_elec(self, ax, hide_xlabel=False, monotone=False, colors=True, legend=True):
        """Plot the electricity balance."""
        dL_e = np.array(p.L_e)[self.dt]
        ix = list(range(len(self.dt)))
        if monotone:
            idx = np.argsort(dL_e)[::-1]
        else:
            idx = ix
        idt = np.array(self.dt)[idx]
        P_e_CHP = self.operation['P_e_CHP'][idt]
        P_e_PV = self.operation['P_e_PV'][idt]
        U_e = self.operation['U_e'][idt]
        V_e = self.operation['V_e'][idt]
        P_h_EB = self.operation['P_h_EB'][idt]
        F_e_EB = [P/p.n_EB for P in P_h_EB]

        if colors:
            clrs = [cl.CHP, cl.PV, cl.GRID, cl.PV2]
        else:
            clrs = [cg.CHP, cg.PV, cg.GRID, cg.PV2]
        stacks = ax.stackplot(
            ix, P_e_CHP, P_e_PV, U_e, V_e,
            labels=['CHP', _('PV (used)'), _('Grid'), _('PV (sold)')],
            colors=clrs, lw=0.2, ls='-', edgecolor=(0, 0, 0, 0.3))
        if not colors:
            hatches = [ht.CHP, ht.PV, ht.GRID, ht.PV2]
            for stack, hatch in zip(stacks, hatches):
                stack.set_hatch(hatch)
        ax.plot(ix, dL_e[idx], color='black', linestyle='-', label=_('Electricity demand'))
        ax.plot(ix, dL_e[idx] + F_e_EB, color='black', linestyle='--', label=_('EB demand'))
        if legend:
            ax.legend(fontsize='small')
            # ax.legend(
                # loc='upper center', bbox_to_anchor=(0.5, -0.3),
                # ncol=3, fancybox=True, fontsize='small'
            # )
        # ax.legend(fontsize='large')
        ax.set_xlim(min(ix), max(ix))
        if not hide_xlabel:
            ax.set_xlabel(_('Hours (h)'))
        ax.set_ylabel(_('Electricity (kW)'))
        # ax.set_title('Production of electricity over 4 days', fontsize='x-large')

    def plot(self, monotone=False, environment=True, fig=None, colors=True):
        """Plot all the figures."""
        if environment:
            if fig is None:
                fig = plt.figure()
            ax1 = plt.subplot(311)
            self._plot_environment(ax1, hide_xlabel=True)
            ax2 = plt.subplot(312, sharex=ax1)
            self._plot_heat(ax2, hide_xlabel=True, monotone=monotone, colors=colors)
            ax2 = plt.subplot(313, sharex=ax1)
            self._plot_elec(ax2, monotone=monotone, colors=colors)
        else:
            if fig is None:
                fig = plt.figure(figsize=(20, 12))
            # ax1 = plt.subplot(111)
            # self._plot_elec(ax1, monotone=monotone, colors=colors)
            ax1 = plt.subplot(211)
            self._plot_heat(ax1, hide_xlabel=True, monotone=monotone, colors=colors)
            ax2 = plt.subplot(212, sharex=ax1)
            self._plot_elec(ax2, monotone=monotone, colors=colors)
        return fig


class ResultMixedTES(Result):
    def __init__(self, objectives, design, operation, storage, *args, **kwargs):
        super().__init__(objectives, design, operation, *args, **kwargs)
        self.storage = storage
        self.dk = list(storage.index)

    def display(self, *args, storage=True, **kwargs):
        """Display a summary of the result."""
        super().display(*args, **kwargs)
        if storage:
            def formatter(v): return f'{v:.3f}'
            print('Storage:')
            text = self.storage.describe().to_string(index=True, float_format=formatter)
            print(text)

    def _plot_environment(self, ax, hide_xlabel=False):
        idx = list(range(len(self.dt)))
        idt = np.array(self.dt)[idx]
        # idk = self.dk
        # T_TES = self.storage['T_TES'][idk]

        # ax.plot(idx, [T[t] for t in idt], color='blue')
        if not hide_xlabel:
            ax.set_xlabel('Hours [h]')
        # ax.set_ylabel('Temperature [°C]', color='blue')
        ax.plot(idx, [p.G[t] for t in idt], color='purple')
        ax.set_ylabel('Solar global radiation [kW/m$^2$]', color='purple')
        # ax2 = ax.twinx()
        # ax2.plot(np.linspace(idx[0], idx[-1], len(idk)), [T_TES[k] for k in idk], color='orange')
        # ax2.set_ylabel('$T_{TES} [°C]$', color='orange')
        # ax2.set_ylim(top=p.T_s)

    def _plot_heat(self, ax, hide_xlabel=False, monotone=False, colors=True, legend=True):
        assert monotone is False, 'Cannot show monotone for TES'
        idx = list(range(len(self.dt)))
        idt = np.array(self.dt)[idx]
        idk = self.dk
        # T_TES = self.storage['T_TES'][idk]
        dL_h = np.array(p.L_h)[self.dt]
        A_ST = self.design['A_ST'][0]
        P_h_CHP = self.operation['P_h_CHP'][idt]
        P_h_TES = self.operation['P_h_TES'][idt]
        # P_h_TES = self.operation['Q_h_TES'][idt]
        # P_h_TES = [P if P >= 0 else 0 for P in P_h_TES]
        P_h_GB = self.operation['P_h_GB'][idt]
        P_h_EB = self.operation['P_h_EB'][idt]
        P_h_ST = self.operation['P_h_ST'][idt]
        # V_TES = self.design['V_TES'][0]
        P_h_ST_unused = [self.P_h_ST_max(A_ST, t) - P_h_ST[t] for t in idt]

        P_h_TES_out = [v if v > 0 else 0 for v in P_h_TES]
        ax.stackplot(idx, P_h_CHP, P_h_GB, P_h_EB, P_h_ST, P_h_TES_out, P_h_ST_unused,
                     labels=['CHP', 'GB', 'EB', _('ST (consumed)'), 'TES', _('ST (not used)')],
                     colors=[cl.CHP, cl.GB, cl.EB, cl.ST, cl.TES, cl.ST2])
        ax.plot(idx, dL_h[idx], color='black', linestyle='-', label=_('Heat demand'))
        # m = 975*V_TES
        # E_h_TES = m*p.c_p_kW*(T_TES - p.T_r)
        E_h_TES = self.storage['E_h_TES'][idk]
        # Remove "negative" energy when the temperature is lower than T_r
        E_h_TES = [v if v > 0 else 0 for v in E_h_TES]
        ax2 = ax.twinx()
        ax2.set_ylabel(_('Heat stored (kWh)'))
        ax2.plot(np.linspace(idx[0], idx[-1], len(idk)), E_h_TES, color='black', linestyle=':', label=_('Heat stored'))
        ax2.set_ylim(bottom=0)
        if not hide_xlabel:
            ax.set_xlabel(_('Hours (h)'))
        ax.set_ylabel(_('Heat (kW)'))

        if legend:
            # ax.legend(
                # loc='upper center', bbox_to_anchor=(0.5, -0.3),
                # ncol=3, fancybox=True, fontsize='small'
            # )
            ax.legend(fontsize='small')
            ax2.legend(loc='upper left', fontsize='small')


class ResultGabrielliTES(Result):
    def _plot_environment(self, ax, hide_xlabel=False):
        idx = list(range(len(self.dt)))
        idt = np.array(self.dt)[idx]
        E_h_TES = self.operation['E_h_TES'][idt]
        S_TES = self.design['S_TES']

        ax.plot(idx, [p.T[t] for t in idt], color='blue')
        if not hide_xlabel:
            ax.set_xlabel('Hours [h]')
        ax.set_ylabel('Temperature [°C]', color='blue')
        ax2 = ax.twinx()
        # m = S_TES/(c_p_kWh*(90-65))
        # convert_temp = lambda E: E/(c_p_kWh*m) + 65
        m = S_TES/(p.c_p_kWh*(90-60))
        def convert_temp(E): return E/(p.c_p_kWh*m) + 60
        ax2.set_ylabel('$T_{TES} [°C]$', color='orange')
        ax2.plot(idx, [convert_temp(E) for E in E_h_TES], color='orange')

    def _plot_heat(self, ax, hide_xlabel=False, monotone=False):
        assert monotone is False, 'Cannot show monotone for TES'
        idx = list(range(len(self.dt)))
        idt = np.array(self.dt)[idx]
        dL_h = np.array(p.L_h)[self.dt]
        A_ST = self.design['A_ST'][0]
        P_h_CHP = self.operation['P_h_CHP'][idt]
        P_h_TES = self.operation['P_h_TES'][idt]
        E_h_TES = self.operation['E_h_TES'][idt]
        P_h_GB = self.operation['P_h_GB'][idt]
        P_h_EB = self.operation['P_h_EB'][idt]
        P_h_ST = self.operation['P_h_ST'][idt]
        P_h_ST_unused = [self.P_h_ST_max(A_ST, t) - P_h_ST[t] for t in idt]

        P_h_TES_out = [v if v > 0 else 0 for v in P_h_TES]
        ax.stackplot(idx, P_h_CHP, P_h_GB, P_h_EB, P_h_ST, P_h_ST_unused, P_h_TES_out,
                     labels=['CHP', 'GB', 'EB', 'ST (consumed)', 'ST (not used)', 'TES'])
        ax.plot(idx, dL_h[idx], color='black', linestyle='-', label='Heat demand')
        ax.legend()
        if not hide_xlabel:
            ax.set_xlabel('Hours [h]')
        ax.set_ylabel('Heat [kW]')
        ax2 = ax.twinx()
        ax2.set_ylabel('Heat stored [kWh]')
        ax2.plot(idx, E_h_TES, color='black', linestyle=':', label='Heat stored')
        ax2.legend(loc='upper left')


class ResultGroup:
    def __init__(self, results=[], arguments={}, duration=None):
        self.results = results
        self.arguments = arguments
        self.duration = duration

    @classmethod
    def load_from_zip(cls, archive):
        """Load the results from a zip file for the old data."""
        with ZipFile(archive) as myzip:
            files = [n for n in myzip.namelist() if n.endswith('.log')]
            files = sorted_alphanumeric(files)
            files = [splitext(f)[0] for f in files]
            with tempfile.TemporaryDirectory() as tmpdir:
                wdir = os.getcwd()
                os.chdir(tmpdir)
                # Load arguments
                myzip.extract('arguments.json')
                with open('arguments.json', 'r') as f_in:
                    arguments = json.load(f_in)
                # Load results
                to_extract = [n for n in myzip.namelist() if n.endswith('operation.csv') or n.endswith('objectives.csv') or n.endswith('dimensioning.csv') or n.endswith('.log')]
                myzip.extractall(members=to_extract)
                results = []
                for fbase in files:
                    results.append(Result.load_from_csv(fbase))
                duration = sum(r.duration for r in results)
                os.chdir(wdir)
        results.reverse()  # The order was reversed for the previous experiments
        return cls(results, arguments, duration)

    def save(self, filename):
        """Save the result to a compressed pickle file."""
        with gzip.open(filename, 'wb') as f:
            pickle.dump(self, f)

    def plot_pareto(self, ax=None, color='blue', style='o--'):
        x = [r.ACR for r in self.results]
        y = [r.RES for r in self.results]
        if ax is None:
            plt.figure()
            ax = plt.subplot(111)
            plot = True
        else:
            plot = False
        ax.plot(x, y, style, color=color)
        ax.set_xlabel(r'$ATCR$')
        ax.set_ylabel(r'$\tau_{EnR}$')
        ax.grid(True)
        if plot:
            plt.show()
        return ax
