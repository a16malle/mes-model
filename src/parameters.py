from scipy.io import loadmat
# from math import pi


data = loadmat('data/data.mat')
T = data['T'].flatten(order='F').tolist()  # temperature (°C)
G = (0.001*data['G']).flatten(order='F').tolist()  # solar global radiation (kW/m^2)
L_e = data['P_elec_TOT_kWh_2016'].flatten().tolist()  # electricity load (kW)
L_h = data['Q_chaleur_TOT_kWh_2016'].flatten().tolist()  # heat load (kW)
del data


# Parameters

a, b, c = 0.1, 0.4, -0.2
n_e_CHP = 0.3  # Fixed value for now (maximum possible efficiency of the CHP)
n_th_CHP = 0.8
a_panel = 1.6
n_dc_ac = 0.9
# U_loss = 9.12*0.001
U_loss = 5*0.001
n_GB = 0.8
n_EB = 0.8
n_ST = 0.8
n_ref = 0.155
beta = 0.43/100
T_ref = 25
P_PV_nom = 0.250  # kW
# P_ST_nom = 0.888  # not used
Fr = 0.9  # What is that?
Cp = 4186
T_in = 20
T_out = 70
T_mean = (T_out + T_in)/2

# Costs
C_gr = ([0.13]*8 + [0.17]*16)*365  # €/kWh over the year (depends on the time of the day) (EDF 2021-10-13)
# C_gr = ([0.12]*8 + [0.13]*12 + [0.15]*4)*365  # €/kWh over the year (depends on the time of the day) (Adihou)
C_g = 0.076  # €/kWh (EDF 2021-10-13)
# C_g = 0.0615  # €/kWh (Adihou)
I_gr = 0.1  # €/kWh

C_inv_CHP = 1140  # €/kW
C_o_CHP_v = 21/1000  # €/kWh

# C_inv_GB = 43  # €/kW (Adihou 2019)
C_inv_GB = 90  # €/kW (Gudmundsson, Thorsen, and Zhang 2013)
C_o_GB_f = 3.15  # €/kW/year (Gudmundsson, Thorsen, and Zhang 2013)
# C_o_GB_v = 4.2/1000  # €/kWh (Adihou 2019)
C_o_GB_v = 0

# C_inv_EB = 32  # €/kW (Adihou 2019)
C_inv_EB = 100  # €/kW (Armila 2020)
C_o_EB_f = 1  # €/kW/year (Armila 2020)
# C_o_EB_v = 3.7/1000  # €/kWh (Adihou 2019)
C_o_EB_v = 0.8/1000  # €/kWh (Armila 2020)

# C_inv_PV = 4130  # €/kW (Adihou 2019)
# C_inv_PV = 1840/1.1422  # USD/kW (IRENA 2021 with 2020 USD/EUR exchange rate of ECB)
C_inv_PV = 1000  # €/kW (Ashfaq and Ianakiev 2018)
# C_o_PV_f = 85  # €/kW/year (Adihou 2019)
# C_o_PV_f = 17.8/1.1422  # €/kW/year (IRENA 2021 with 2020 USD/EUR exchange rate of ECB)
C_o_PV_f = 15  # €/kW/year (Ashfaq and Ianakiev 2018)

C_inv_ST = 615  # €/kW
C_o_ST_f = 10  # €/m^2/year

# C_inv_TES = 7.7  # €/m3 (Wrong value!!!)
# C_o_TES_f = 0.3  # €/m3/year (Wrong value!!!)
C_inv_TES = 7.69  # €/kWh
C_o_TES_f = 0.32  # €/kWh/year

kr = 0.05  # interest rate
n = 20  # 20 years
crf = kr*(1 + kr)**n/((1 + kr)**n - 1)

# TES
c_p = 4182  # specific heat capacity of water (J.kg-1.K-1)
c_p_kW = c_p/1000  # (kW.s.kg-1.K-1)
c_p_kWh = c_p*2.78e-7  # (kW.h.kg-1.K-1)
T_s = 90  # supply temperature
T_r = 60  # return temperature
K_s = 0.001*1.1189  # heat loss factor
# V = 40  # tank volume (m^3)
# V = 3000/(975*c_p_kW*(T_s - T_r))  # tank volume (m^3)
# r = (V/2*pi)**(1/3)  # radius of the tank
# h = V/(pi*r**2)  # height of the tank
# A = 2*pi*r**2 + 2*pi*r*h  # surface area of the tank
# m = 975*V  # mass of water for a temperature of 75°C
delta_k = 600  # time step TES
dot_m_max = 7.85  # max mass flow rate
k_w = 0.660  # thermal conductivity of water around 70°C

radius_min = 1
radius_max = 2.5  # max radius of the tank

F_h_TES_max = dot_m_max*c_p_kW*(T_s - min(T))
P_h_TES_max = dot_m_max*c_p_kW*(T_s - T_r)
