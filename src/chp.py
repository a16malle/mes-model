import gurobipy as gp
from gurobipy import GRB
import numpy as np
from abc import ABC, abstractmethod

from . import parameters as p


class CHP(ABC):
    """Class for the CHP model"""
    def __init__(self, **kwargs):
        self._constraints = []
        self.P_CHP_nom = None
        self.P_e_CHP = None
        self.F_g_CHP = None
        self.P_h_CHP = None
        self.dt = None

    def set_timesteps(self, timesteps):
        self.dt = timesteps

    def add_variables(self, model):
        self.P_CHP_nom = model.addVar(100, 1000, name='P_CHP_nom')
        self.P_e_CHP = model.addVars(self.dt, lb=0, ub=1000, name='P_e_CHP')
        self.F_g_CHP = model.addVars(self.dt, lb=0, name='F_g_CHP')
        self.P_h_CHP = model.addVars(self.dt, lb=0, name='P_h_CHP')

    @abstractmethod
    def add_constraints(self, model):
        dt = self.dt
        model.addConstrs((self.P_e_CHP[t] <= self.P_CHP_nom for t in dt))


class NonlinearCHP(CHP):
    """Model a non-linear CHP through quadratic constraints"""
    def add_constraints(self, model):
        super().add_constraints(model)

        F_g_CHP = self.F_g_CHP
        P_e_CHP = self.P_e_CHP
        P_CHP_nom = self.P_CHP_nom
        dt = self.dt

        z = model.addVars(dt, lb=0, ub=1)
        z2 = model.addVars(dt, lb=0, ub=1)
        model.addConstrs((P_CHP_nom*z[t] == P_e_CHP[t] for t in dt))
        model.addConstrs((z2[t] == z[t]*z[t] for t in dt))
        model.addConstrs((F_g_CHP[t]*(p.a + p.b*z[t] + p.c*z2[t]) == P_e_CHP[t] for t in dt))
        model.setParam('NonConvex', 2)


class LinearCHP(CHP):
    def add_constraints(self, model):
        super().add_constraints(model)

        F_g_CHP = self.F_g_CHP
        P_e_CHP = self.P_e_CHP
        dt = self.dt
        c = self._constraints

        c.extend(model.addConstrs((F_g_CHP[t] == P_e_CHP[t]/p.n_e_CHP for t in dt)).values())


class AdaptedCHPSOS(CHP):
    """Adapted triangle linearization"""
    def __init__(self, N=2):
        if N < 2:
            raise ValueError(f'The CHP needs at least 2 breakpoints N, {N} given')
        super().__init__()
        self.N = N
        self.lp_x = np.array([100, 1000])  # Samples for P_CHP_nom
        self.lp_y = np.linspace(0, 1000, N)  # Samples for P_e_CHP

    def add_variables(self, model):
        super().add_variables(model)
        self.lp_alpha = model.addVars(self.dt, self.N, lb=0, ub=1, name='alpha')

    def lp_f(self, xi, yi):
        """Function of the fuel consumption for the piece-wise linearization"""
        P_nom = self.lp_x[xi]
        P = self.lp_y[yi]
        if P_nom == 0:
            return 0
        n_e = p.a + p.b*P/P_nom + p.c*(P/P_nom)**2
        return P/n_e

    def add_constraints(self, model):
        super().add_constraints(model)

        P_CHP_nom = self.P_CHP_nom
        P_e_CHP = self.P_e_CHP
        F_g_CHP = self.F_g_CHP
        dt = self.dt
        c = self._constraints

        for t in dt:
            c.append(model.addSOS(GRB.SOS_TYPE2, [self.lp_alpha[(t, i)] for i in range(self.N)]))
        c.extend(model.addConstrs((self.lp_alpha.sum(t, '*') <= 1 for t in dt)))
        x_constr = (P_CHP_nom == self.lp_x[1]*self.lp_alpha.sum(t, '*') for t in dt)
        c.extend(model.addConstrs(x_constr))
        a1_constr = (P_e_CHP[t] == gp.quicksum(self.lp_alpha[(t, i)]*self.lp_y[i]
                     for i in range(self.N)) for t in dt)
        c.extend(model.addConstrs(a1_constr))
        a2_constr = (F_g_CHP[t] == gp.quicksum(self.lp_alpha[(t, i)]*self.lp_f(1, i)
                     for i in range(self.N)) for t in dt)
        c.extend(model.addConstrs(a2_constr))


class AdaptedCHP(CHP):
    """Adapted triangle linearization"""
    def __init__(self, N=2, constraints=None):
        if N < 2:
            raise ValueError(f'The CHP needs at least 2 breakpoints N, {N} given')
        super().__init__()
        self.N = N
        self.lp_x = np.array([100, 1000])  # Samples for P_CHP_nom
        self.lp_y = np.linspace(0, 1000, N)  # Samples for P_e_CHP

    def add_variables(self, model):
        super().add_variables(model)
        self.lp_alpha = model.addVars(self.dt, self.N, lb=0, ub=1, name='alpha')
        self.lp_h = model.addVars(self.dt, self.N - 1, lb=0, ub=1, vtype=GRB.BINARY, name='h')

    def lp_f(self, xi, yi):
        """Function of the fuel consumption for the piece-wise linearization"""
        P_nom = self.lp_x[xi]
        P = self.lp_y[yi]
        if P_nom == 0:
            return 0
        n_e = p.a + p.b*P/P_nom + p.c*(P/P_nom)**2
        return P/n_e

    def add_constraints(self, model):
        super().add_constraints(model)

        P_CHP_nom = self.P_CHP_nom
        P_e_CHP = self.P_e_CHP
        F_g_CHP = self.F_g_CHP
        dt = self.dt
        c = self._constraints

        for t in dt:
            c.append(model.addConstr(self.lp_alpha[(t, 0)] <= self.lp_h[(t, 0)]))
            c.append(model.addConstr(self.lp_alpha[(t, self.N - 1)] <= self.lp_h[(t, self.N - 2)]))
            for i in range(1, self.N - 1):
                expr = gp.LinExpr([1, 1], [self.lp_h[(t, i)], self.lp_h[(t, i-1)]])
                constr = self.lp_alpha[(t, i)] <= expr
                c.append(model.addConstr(constr))
        c.extend(model.addConstrs((self.lp_h.sum(t, '*') == 1 for t in dt)).values())
        c.extend(model.addConstrs((self.lp_alpha.sum(t, '*') <= 1 for t in dt)).values())
        c.extend(model.addConstrs(
            (P_CHP_nom == self.lp_x[1]*self.lp_alpha.sum(t, '*') for t in dt)).values())
        c.extend(model.addConstrs(
            (P_e_CHP[t] == gp.quicksum(self.lp_alpha[(t, i)]*self.lp_y[i]
             for i in range(self.N)) for t in dt)).values())
        c.extend(model.addConstrs(
            (F_g_CHP[t] == gp.quicksum(self.lp_alpha[(t, i)]*self.lp_f(1, i)
             for i in range(self.N)) for t in dt)).values())


# class TriangleCHP(CHP):
    # """D'Ambrosio's triangle linearization"""
    # def __init__(self, mes, N=2, M=2):
        # assert N >= 2
        # assert M >= 2
        # super().__init__(mes)
        # self.N = N
        # self.M = M
        # self.lp_x = np.linspace(0, 1000, N)  # Samples for P_CHP_nom
        # self.lp_y = np.linspace(0, 1000, M)  # Samples for P_e_CHP
        # self.lp_alpha = LpVariable.dicts('alpha', product(mes.dt, range(N), range(M)), 0, 1)
        # self.lp_h_u = LpVariable.dicts(
            # 'h_u', product(mes.dt, range(N-1), range(M-1)), 0, cat='Binary')
        # self.lp_h_l = LpVariable.dicts(
            # 'h_u', product(mes.dt, range(N-1), range(M-1)), 0, cat='Binary')
