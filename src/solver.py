import time
import copy
import numpy as np
from collections import defaultdict
from gurobipy import GRB
from abc import ABC, abstractmethod
import logging

from .utils import bcolors as ts, counter, pbar_manager
from .exceptions import NonOptimal
from .results import ResultGroup
from .mes import MESMetaheuristic
from .chp import AdaptedCHP
from .metaheuristic import GeneticAlgorithm


logger = logging.getLogger(__name__)


class MultiOut:
    def __init__(self, *args):
        """
        Handler to write to multiple writers.

        Allows for example to write to the screen and to files at the same time.

        Args:
            *args: all the different writers.
        """
        self.writers = args

    def write(self, s):
        for f in self.writers:
            f.write(s)

    def flush(self):
        for f in self.writers:
            f.flush()


class Solver:
    def __init__(self, mes, epsilon_gap=1e-6, epsilons=None):
        """
        MILP/LP solver for the MES.

        Args:
            epsilon_gap: gap for the weighted sum of the objective functions. Default: 1e-6.
        """
        self.mes = mes
        self.epsilon_constraint = None
        self._epsilon_gap = epsilon_gap
        self._f1 = None
        self._f2 = None
        self._f1_name = None
        self._f2_name = None
        self._gaps = []
        self.epsilons = epsilons

    @property
    def f1(self):
        return self._f1

    @property
    def f1_name(self):
        return self._f1_name

    @property
    def f2(self):
        return self._f2

    @property
    def f2_name(self):
        return self._f2_name

    def _set_objectives(self, prioritizeRES=False, epsilon=None):
        """
        Set the objective functions with an epsilon constraint value.

        Args:
            prioritizeRES: wether the main objective function is RES or not. Default: False.
            epsilon: value of the epsilon constraint, or None if there is no epsilon constraint. Default: None.
        """
        if self.mes.ACR is None or self.mes.RES is None:
            self.mes.init_objectives()
        # Choose which function is prioritized
        if prioritizeRES:
            # self._f1 = self.RES; self._f2 = self.ACR
            self._f1 = (1 - self._epsilon_gap)*self.mes.RES + self._epsilon_gap*self.mes.ACR
            self._f2 = self.mes.ACR
            self._f1_name = 'RES'
            self._f2_name = 'ACR'
        else:
            self._f1 = self.mes.ACR
            self._f2 = self.mes.RES
            self._f1_name = 'ACR'
            self._f2_name = 'RES'

        # Remove old epsilon constraint
        if self.epsilon_constraint is not None:
            self.mes.m.remove(self.epsilon_constraint)

        # Epsilon constraint
        if epsilon is not None:
            if prioritizeRES:
                self.epsilon_constraint = self.mes.m.addConstr(self.mes.ACR >= epsilon, 'Epsilon-constraint')
            else:
                self.epsilon_constraint = self.mes.m.addConstr(self.mes.RES >= epsilon, 'Epsilon-constraint')

        # Objective functions
        self.mes.m.setObjective(self.f1)

    def init_variables(self):
        """
        Initialize the variables of the model.

        The types of variables can be different for different resolution methods.
        For example a binary variable can be relaxed.
        """
        self.mes.init_variables()

    def set_constraints(self):
        """
        Set the constraints of the model.

        The constraints can be different for different resolution methods.
        """
        self.mes.set_constraints()

    def _optimize(self, prioritizeRES, epsilon):
        def data_cb(model, where):
            if where == GRB.Callback.MIP:
                cur_obj = model.cbGet(GRB.Callback.MIP_OBJBST)
                cur_bnd = model.cbGet(GRB.Callback.MIP_OBJBND)
                self._gaps.append([time.time(), cur_obj, cur_bnd])

        self.mes.m.optimize(callback=data_cb)
        if self.mes.m.getAttr('Status') == GRB.TIME_LIMIT:
            logger.info('Time limit reached')
        elif self.mes.m.getAttr('Status') != GRB.OPTIMAL:
            raise NonOptimal

    def optimize(self, prioritizeRES=False, epsilon=None):
        self._gaps = [[time.time(), 0, float('inf')]]
        logger.info('Starting optimization...')
        self._set_objectives(prioritizeRES, epsilon)
        start = time.time()
        self._optimize(prioritizeRES, epsilon)
        duration = time.time() - start
        try:
            solution = [n for t, n in self.mes.chp.lp_h.keys() if self.mes.chp.lp_h[(t, n)].x == 1]
        except AttributeError:
            solution = None
        # logger.debug(f'{self.f1} {self.f1.getValue()}')
        # self.mes.m.write(f'{time.time()}.lp')
        self._gaps.append([time.time(), self.mes.m.objVal, self.mes.m.objBound])
        return self.mes.to_result(duration=duration, solution=solution, gaps=self._gaps[:])

    def optimize_pareto(self, number=10):
        """
        Optimize on the Pareto front.

        Args:
            number: number of solutions on the Pareto front. Default: 10.
        """
        results = []
        if number > 1:
            logger.info(f'{ts.WARNING}#### Optimization 1/{number} (maximize ACR) ####{ts.ENDC}')
        first = self.optimize()
        results.append(first)
        if number > 1:
            logger.info(f'{ts.WARNING}#### Optimization 2/{number} (maximize RES) ####{ts.ENDC}')
            last = self.optimize(prioritizeRES=True)
            results.append(last)
            samples = np.linspace(first.RES, last.RES, number)
            samples = samples[1:-1]
            for i, constraint in enumerate(samples):
                if self.epsilons is not None and len(self.epsilons) == len(samples):
                    constraint = self.epsilons[i]
                logger.info(f'{ts.WARNING}#### Optimization {i+3}/{number} (maximize ACR, RES >= {constraint}) ####{ts.ENDC}')
                result = self.optimize(epsilon=constraint)
                results.append(result)
            results.sort(key=lambda r: r.ACR)
        duration = sum((r.duration for r in results))
        return ResultGroup(results, duration=duration)


class HeuristicSolver(Solver):
    def __init__(self, *args, **kwargs):
        """Heuristic solver for the MILP with the CHP."""
        super().__init__(*args, **kwargs)
        assert isinstance(self.mes.chp, AdaptedCHP), 'The heuristic only works with the adapted CHP'
        self.initialized = False
        self.gap_improvement = 0  # Improvement of the local search

    def _set_chp_constraints(self, constraints=None):
        """Remove the old constraints and set new CHP constraints."""
        if not self.initialized:
            self.initialized = True
            self.mes.chp._constraints = []

        self.mes.m.remove(self.mes.chp._constraints)
        self.mes.chp._constraints = []
        if constraints is not None:
            c = self.mes.chp._constraints
            for key, value in constraints.items():
                c.append(self.mes.m.addConstr(self.mes.chp.lp_h[key] == value))

    def _get_relaxed_constraints(self, constraints={}):
        """Choose the constraints for the relaxed CHP."""
        constraints = {}
        dt = self.mes.dt
        dn = list(range(self.mes.chp.N - 1))
        for t in dt:
            current = {}
            for n in dn:
                current[(t, n)] = self.mes.chp.lp_h[(t, n)].x
            hk = max(current, key=current.get)  # key for highest value
            constraints[hk] = 1
        return constraints

    def _solve_initial(self):
        """Find an initial solution."""
        self.mes.m.optimize()
        if self.mes.m.getAttr('Status') != GRB.OPTIMAL:
            raise NonOptimal
        constraints = self._get_relaxed_constraints()
        self._set_chp_constraints(constraints=constraints)
        self.mes.m.optimize()
        if self.mes.m.getAttr('Status') != GRB.OPTIMAL:
            raise NonOptimal

    def _get_objectives(self):
        """Return the values of the objective functions."""
        obj1 = self.f1.getValue()
        obj2 = self.f2.getValue()
        return (obj1, obj2)

    def _get_solution(self):
        """Return the triangles selected."""
        sol = [n for t, n in self.mes.chp.lp_h.keys() if self.mes.chp.lp_h[(t, n)].x == 1]
        return sol

    def _check_solution(self, solution, best_objectives):
        """Check if the current solution is better than the previous best."""
        def to_constraints(s): return {(t, n): 1 for t, n in zip(self.mes.dt, s)}
        constraints = to_constraints(solution)
        self._set_chp_constraints(constraints)
        self.mes.m.optimize()
        obj1 = self.f1.getValue()
        obj2 = self.f2.getValue()
        gap = self.gap_improvement
        if obj1 - best_objectives[0] > gap or obj1 >= best_objectives[0] and obj2 - best_objectives[1] > gap:
            better = True
        else:
            better = False
        return better, (obj1, obj2)

    def _update_solution(self, solution):
        for i, t in enumerate(self.mes.dt):
            n = solution[i]
            if n < self.mes.chp.N - 2 and self.mes.chp.lp_alpha[(t, n)].x == 0:
                solution[i] += 1
            elif n > 0 and self.mes.chp.lp_alpha[(t, n + 1)].x == 0:
                solution[i] -= 1

    def _iterator_heuristic(self):
        iteration = 0
        self.mes.m.setParam('LogToConsole', False)
        self.mes.relax_model()
        self._solve_initial()
        self.mes.force_binary()
        name_obj = (self.f1_name, self.f2_name)
        best_obj = self._get_objectives()
        best_sol = self._get_solution()
        iteration += 1
        yield iteration, best_obj, name_obj
        while True:
            current = copy.copy(best_sol)
            self._update_solution(current)
            better, objectives = self._check_solution(current, best_obj)
            if better:
                best_sol = current
                best_obj = objectives
            iteration += 1
            yield iteration, best_obj, name_obj
            if not better:
                break
        better, objectives = self._check_solution(current, best_obj)
        yield iteration, best_obj, name_obj

    def _run_heuristic(self):
        log = logger.getChild('heuristic')
        start = time.time()
        leave = not pbar_manager.exists()
        c_format = u'{desc}{desc_pad}{count:d} {unit}{unit_pad}' + \
                   u'(best: {best_str})' + \
                   u'{desc_pad}[{elapsed}, {rate:.2f}{unit_pad}{unit}/s]{fill}'
        with counter(title='Heuristic', desc='Heuristic', unit='iterations', leave=leave,
                     counter_format=c_format) as pbar:
            for iteration, best_obj, name_obj in self._iterator_heuristic():
                best_str = ', '.join(f'{k}={v:.3f}' for k, v in zip(name_obj, best_obj))
                pbar.update(best_str=best_str)
                duration = time.time() - start
                log.debug('({0[0]}: {1[0]:.3f}, {0[1]}: {1[1]:.3f}) in {2:.2f}s'.format(name_obj, best_obj, duration))
            log.debug(f'Finished in {iteration+1} iterations. Obj: {best_obj[0]} in {duration:.2f}s')

    def _optimize(self, prioritizeRES=False, epsilon=None):
        self._run_heuristic()
        if self.mes.m.getAttr('Status') != GRB.OPTIMAL:
            raise NonOptimal


class GeneticAlgorithmABC(ABC):
    def __init__(self, mes, epsilon_gap=1e-6, size=100,
                 nbr_iterations=100, nbr_no_improvement=None, nodes=1,
                 crossover_weights=(0.67, 0.33), sigma_factor=1,
                 elite_percent=0.2, seed=None):
        assert isinstance(mes, MESMetaheuristic), 'The MES should be MESMetaheuristic'
        super().__init__(mes=mes, epsilon_gap=epsilon_gap)
        self.ga = GeneticAlgorithm(self, nbr_iterations=nbr_iterations,
                                   nbr_no_improvement=nbr_no_improvement,
                                   nodes=nodes,
                                   crossover_weights=crossover_weights,
                                   sigma_factor=sigma_factor,
                                   elite_percent=elite_percent, seed=seed)
        self.ga.init_random_population(size)

    def add_solution(self, F, P, r):
        sol = self.ga.create_from_power(F, P, r)
        self.ga.population.append(sol)

    def _get_variables(self, individual):
        """Return the temperature, the power, and if the storage is discharging."""
        radius = individual.radius
        T_TES = individual.temperature
        F_h_TES = defaultdict(lambda: 0)
        P_h_TES = defaultdict(lambda: 0)
        for it, t in enumerate(self.mes.dt):
            F_h_TES[t] = individual.F_in[it]
            P_h_TES[t] = individual.P_out[it]
        return radius, P_h_TES, F_h_TES, T_TES

    def calculate_objective_functions(self, solution):
        self._optimize_solution(solution)
        obj1 = self.f1.getValue()
        obj2 = self.f2.getValue()
        return (obj1, obj2)

    @abstractmethod
    def _optimize_solution(self, solution):
        pass

    def _optimize(self, prioritizeRES=False, epsilon=None):
        self.mes.m.setParam('LogToConsole', False)
        self.ga.solve()
        ordered = self.ga.get_ordered()
        self._optimize_solution(ordered[0])

    def get_results(self, number):
        results = []
        for i, solution in enumerate(self.ga.get_ordered()):
            if i >= number:
                break
            self._optimize_solution(solution)
            result = self.mes.to_result(solution=solution)
            results.append(result)
        return results


class GASolver(GeneticAlgorithmABC, Solver):
    def _optimize_solution(self, solution):
        radius, P, F, T = self._get_variables(solution)
        self.mes.update_constraints(radius, P, F, T)
        # self.mes.m.presolve()
        self.mes.m.optimize()
        if self.mes.m.getAttr('Status') != GRB.OPTIMAL:
            raise NonOptimal


class GAHeuristicSolver(GeneticAlgorithmABC, HeuristicSolver):
    def _optimize_solution(self, solution):
        # TODO: Save the results
        radius, P, F, T = self._get_variables(solution)
        self.mes.update_constraints(radius, P, F, T)
        super()._run_heuristic()
