import gurobipy as gp
from gurobipy import GRB
import pandas as pd
import numpy as np
from math import pi
import logging

from . import parameters as p
from .results import Result, ResultMixedTES, ResultGabrielliTES, Run
from .chp import LinearCHP


logger = logging.getLogger(__name__)


def volume_to_energy(V):
    """Convert the volume of the TES to energy."""
    m = 975*V  # mass of water for a temperature of 75°C
    return m*p.c_p_kWh*(p.T_r - p.T_r)


class MES():
    def __init__(self, dt=list(range(8760)), chp=LinearCHP()):
        self.variables = {}
        self.dt = dt

        m = gp.Model('MES')
        # Set a limit (in GB) for the nodes size
        m.setParam('NodefileStart', 30)
        # m.setParam('MIPGapAbs', 0)
        # m.setParam('MIPGap', 0)
        # Set the log interval (in s) when there is no improvement
        m.setParam('DisplayInterval', 30)
        m.setParam('Presolve', 0)
        m.setAttr('ModelSense', GRB.MAXIMIZE)
        self.m = m

        self.ACR = None
        self.RES = None
        self.result_cls = Result
        self.chp = chp
        self.chp.set_timesteps(self.dt)

        self.elect_balance = None
        self.heat_balance = None

    def init_variables(self):
        """Initialize the variables of the model."""
        logger.info('Initializing variables...')
        dt = self.dt
        m = self.m
        self.chp.add_variables(self.m)
        self.P_CHP_nom = self.chp.P_CHP_nom
        self.P_GB_nom = m.addVar(100, 3000, name='P_GB_nom')
        self.P_EB_nom = m.addVar(100, 3000, name='P_EB_nom')
        self.A_PV = m.addVar(0, 10000, name='A_PV')
        self.A_ST = m.addVar(0, 10000, name='A_ST')

        self.P_e_CHP = self.chp.P_e_CHP
        self.F_g_CHP = self.chp.F_g_CHP
        self.P_h_CHP = self.chp.P_h_CHP
        self.P_h_GB = m.addVars(dt, lb=0, ub=3000, name='P_h_GB')
        self.P_h_EB = m.addVars(dt, lb=0, ub=3000, name='P_h_EB')
        self.P_h_ST = m.addVars(dt, lb=0, name='P_h_ST')
        self.P_e_PV = m.addVars(dt, lb=0, name='P_e_PV')
        self.U_e = m.addVars(dt, lb=0, name='U_e')
        self.V_e = m.addVars(dt, lb=0, name='V_e')

    def P_h_ST_max(self, t):
        """Return the total energy producted by ST."""
        coeff = p.G[t]*p.n_ST - p.U_loss*(p.T_mean - p.T[t])
        if coeff < 0:
            coeff = 0
        return self.A_ST*coeff

    def P_e_PV_max(self, t):
        """Return the total energy produced by PV."""
        T_cell = 30 + 0.0175*(1000*p.G[t] - 300) + 1.14*(p.T[t] - 25)
        n_PV = p.n_ref*(1 - p.beta*(T_cell - p.T_ref))
        P_PV = self.A_PV*p.n_dc_ac*n_PV*p.G[t]
        return P_PV

    def _set_model_constraints(self):
        """Set the constraints of the model except those of the CHP."""
        dt = self.dt
        self.m.addConstr(self.A_PV + self.A_ST <= 10000, 'Max area of solar panels')
        self.m.addConstrs(
            (self.P_h_CHP[t] <= p.n_th_CHP*(self.F_g_CHP[t] - self.P_e_CHP[t]) for t in dt))
        self.m.addConstrs((self.P_h_GB[t] <= self.P_GB_nom for t in dt))
        self.m.addConstrs((self.P_h_EB[t] <= self.P_EB_nom for t in dt))
        self.m.addConstrs((self.V_e[t] + self.P_e_PV[t] == self.P_e_PV_max(t) for t in dt))
        self.m.addConstrs((self.P_h_ST[t] <= self.P_h_ST_max(t) for t in dt))
        self.elec_balance = self.m.addConstrs(
            (self.P_e_CHP[t] + self.P_e_PV[t] + self.U_e[t]
                - self.P_h_EB[t]/p.n_EB == p.L_e[t] for t in dt),
            'electricity_balance')
        self.heat_balance = self.m.addConstrs(
            (self.P_h_CHP[t] + self.P_h_GB[t] + self.P_h_EB[t]
                + self.P_h_ST[t] == p.L_h[t] for t in dt),
            'heat_balance')

    def _set_chp_constraints(self):
        """Set the CHP constraints."""
        self.chp.add_constraints(self.m)

    def set_constraints(self):
        """Set all constraints of the model."""
        logger.info('Adding constraints...')
        self._set_model_constraints()
        self._set_chp_constraints()
        # self.m.setParam('MIPGap', 0.1)  # To use a solver gap

    def _get_costs(self):
        """Return the costs ATC_sys and ATC_ref."""
        dt = self.dt
        dt_ratio = len(dt)/8760
        N_PV = self.A_PV*1/p.a_panel

        # C_o_ref = p.C_o_GB_f*max(p.L_h)*dt_ratio  # ERROR: Should be the max over the time steps
        # C_inv_ref = p.C_inv_GB*max(p.L_h)  # ERROR: Should be the max over the time steps
        max_L_h = max([p.L_h[t] for t in self.dt])
        C_o_ref = p.C_o_GB_f*max_L_h*dt_ratio
        C_inv_ref = p.C_inv_GB*max_L_h
        C_gr_ref = sum([p.C_gr[t]*p.L_e[t] for t in dt])
        C_g_ref = p.C_g*sum([p.L_h[t]/p.n_GB for t in dt])
        AOC_ref = C_o_ref + C_gr_ref + C_g_ref
        ATC_ref = AOC_ref + p.crf*C_inv_ref*dt_ratio

        C_o_CHP = p.C_o_CHP_v*self.P_e_CHP.sum()
        C_o_GB = p.C_o_GB_v*self.P_h_GB.sum() + p.C_o_GB_f*self.P_GB_nom*dt_ratio
        C_o_EB = p.C_o_EB_v*self.P_h_EB.sum() + p.C_o_EB_f*self.P_EB_nom*dt_ratio
        C_o_PV = p.C_o_PV_f*p.P_PV_nom*N_PV*dt_ratio
        C_o_ST = p.C_o_ST_f*self.A_ST*dt_ratio
        C_o_sys = C_o_CHP + C_o_GB + C_o_EB + C_o_PV + C_o_ST

        C_inv_sys = (p.C_inv_CHP*self.P_CHP_nom + p.C_inv_GB*self.P_GB_nom
                     + p.C_inv_EB*self.P_EB_nom + p.C_inv_PV*p.P_PV_nom*N_PV
                     + p.C_inv_ST*self.A_ST)

        I_gr_sys = p.I_gr*self.V_e.sum()
        C_gr_sys = gp.quicksum(p.C_gr[t]*self.U_e[t] for t in self.dt)
        # C_g_sys = gp.quicksum(C_g*(F_g_CHP[t] + P_h_GB[t]/n_GB) for t in dt)
        C_g_sys = p.C_g*(self.F_g_CHP.sum() + self.P_h_GB.sum()/p.n_GB)

        AOC_sys = C_o_sys - I_gr_sys + C_gr_sys + C_g_sys
        ATC_sys = AOC_sys + p.crf*C_inv_sys*dt_ratio
        return ATC_sys, ATC_ref

    def init_objectives(self):
        """Initialize the objective functions of the model."""
        (ATC_sys, ATC_ref) = self._get_costs()

        self.ACR = 100*(1 - ATC_sys/ATC_ref)

        P_RES = self.P_e_PV.sum() + self.P_h_ST.sum()
        P_cons = sum(p.L_e[t] + p.L_h[t] for t in self.dt)

        self.RES = 100*P_RES/P_cons

    def relax_model(self):
        """Relax the binary variables and remove the SOS constraints."""
        # Use a logger instead of prints
        self.m.update()
        count_binaries = 0
        self.modified = []
        for v in self.m.getVars():
            if v.getAttr('VType') == GRB.BINARY:
                v.setAttr('VType', GRB.CONTINUOUS)
                self.modified.append(v)
                count_binaries += 1
        sos = self.m.getSOSs()
        count_sos = len(sos)
        self.m.remove(sos)
        logger.debug(f'Changed {count_binaries} binary variables and '
                     f'removed {count_sos} SOS constraints')

    def force_binary(self):
        self.m.update()
        for v in self.modified:
            v.setAttr('VType', GRB.BINARY)

    def _get_objectives_dict(self):
        return {
            'ACR': [self.ACR.getValue()],
            'RES': [self.RES.getValue()]
        }

    def _get_design_dict(self):
        return {
            'P_CHP_nom': [self.P_CHP_nom.x],
            'Q_GB_nom': [self.P_GB_nom.x],
            'Q_EB_nom': [self.P_EB_nom.x],
            'A_PV': [self.A_PV.x],
            'A_ST': [self.A_ST.x]
        }

    def _get_operation_dict(self):
        dt = self.dt
        return {
            'dt': dt,
            'P_e_CHP': [self.P_e_CHP[t].x for t in dt],
            'F_g_CHP': [self.F_g_CHP[t].x for t in dt],
            'P_h_CHP': [self.P_h_CHP[t].x for t in dt],
            'P_h_GB': [self.P_h_GB[t].x for t in dt],
            'P_h_EB': [self.P_h_EB[t].x for t in dt],
            'P_h_ST': [self.P_h_ST[t].x for t in dt],
            'P_e_PV': [self.P_e_PV[t].x for t in dt],
            'U_e': [self.U_e[t].x for t in dt],
            'V_e': [self.V_e[t].x for t in dt]
        }

    def to_result(self, duration=None, **kwargs):
        # if self.m.getAttr('Status') != GRB.OPTIMAL:
            # raise Exception('The model was not solved to optimality')

        obj_dict = self._get_objectives_dict()
        des_dict = self._get_design_dict()
        ope_dict = self._get_operation_dict()

        objectives = pd.DataFrame(obj_dict)
        design = pd.DataFrame(des_dict)
        operation = pd.DataFrame(ope_dict)
        operation = operation.set_index('dt')
        run = Run(
            Status=self.m.getAttr('Status'),
            Runtime=self.m.getAttr('Runtime'),
        )
        kwargs['C_ref_fixed'] = True
        logger.info(f'ACR: {self.ACR.getValue()}, RES: {self.RES.getValue()}, duration: {duration}')
        result = self.result_cls(
            objectives, design, operation, run=run, duration=duration, **kwargs)
        return result


class MESWithGabrielliTES(MES):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.S_TES = self.m.addVar(100, 3000, name='S_TES')

        self.P_h_TES = self.m.addVars(self.dt, lb=-3000/4, ub=3000/4, name='P_h_TES')
        self.E_h_TES = self.m.addVars(self.dt, lb=0, ub=3000, name='E_h_TES')

        self.result_cls = ResultGabrielliTES

    def _set_model_constraints(self):
        dt = self.dt
        self.m.addConstr(self.A_PV + self.A_ST <= 10000, 'Max area of solar panels')
        self.m.addConstrs((self.P_h_CHP[t] <= p.n_th_CHP*(self.F_g_CHP[t] - self.P_e_CHP[t]) for t in dt))
        self.m.addConstrs((self.P_e_CHP[t] <= self.P_CHP_nom for t in dt))  # v1 LP: P_e <= P_nom for all t
        # self.m.addGenConstrMax(self.P_CHP_nom, self.P_e_CHP, 100)  # v2 MIP: P_nom = max(P_e, constant)
        self.m.addConstrs((self.P_h_GB[t] <= self.P_GB_nom for t in dt))  # v1 LP: P_h <= P_nom for all t
        # self.m.addGenConstrMax(self.P_GB_nom, self.P_h_GB, 100)  # v2 MIP: P_nom = max(P_h, constant)
        self.m.addConstrs((self.P_h_EB[t] <= self.P_EB_nom for t in dt))  # v1 LP: P_h <= P_nom for all t
        # self.m.addGenConstrMax(self.P_EB_nom, self.P_h_EB, 100)  # v2 MIP: P_nom = max(P_h, constant)
        self.m.addConstrs((self.E_h_TES[t] <= self.S_TES for t in dt))
        self.m.addConstrs((self.V_e[t] + self.P_e_PV[t] == self.P_e_PV_max(t) for t in dt))
        self.m.addConstrs((self.P_h_ST[t] <= self.P_h_ST_max(t) for t in dt))
        self.m.addConstrs((self.P_h_TES[t] >= -self.S_TES/4 for t in dt))
        self.m.addConstrs((self.P_h_TES[t] <= self.S_TES/4 for t in dt))
        self.m.addConstr(self.E_h_TES[dt[0]] == 0)
        # self.m.addConstrs((self.E_h_TES[t] == self.E_h_TES[t-1]*(1 - 0.005*1) - (0.001*self.S_TES*(65-T[t])/(90-65) + 0.95*self.P_h_TES[t])*1 for t in dt[1:]))
        self.m.addConstrs((self.E_h_TES[t] == self.E_h_TES[t-1]*(1 - 0.005*1) - (0.001*self.S_TES*(60-p.T[t])/(90-60) + 0.95*self.P_h_TES[t])*1 for t in dt[1:]))
        self.elec_balance = self.m.addConstrs(
            (self.P_e_CHP[t] + self.P_e_PV[t] + self.U_e[t]
                - self.P_h_EB[t]/p.n_EB == p.L_e[t] for t in dt),
            'electricity_balance')
        self.heat_balance = self.m.addConstrs(
            (self.P_h_CHP[t] + self.P_h_GB[t] + self.P_h_EB[t]
                + self.P_h_ST[t] + self.P_h_TES[t] == p.L_h[t] for t in dt),
            'heat_balance')
        self.chp.add_constraints(self.m)

    def _get_costs(self):
        dt = self.dt
        dt_ratio = len(dt)/8760
        (ATC_sys, ATC_ref) = super()._get_costs()
        ATC_sys += 0.02*10.5*p.crf*self.S_TES*len(dt)/8760  # TES operation costs
        ATC_sys += p.crf*dt_ratio*(10.5*self.S_TES + 0)  # TES investment costs
        return ATC_sys, ATC_ref

    def _get_design_dict(self):
        des_dict = super()._get_design_dict()
        des_dict['S_TES'] = [self.S_TES.x]
        return des_dict

    def _get_operation_dict(self):
        ope_dict = super()._get_operation_dict()
        ope_dict['P_h_TES'] = [self.P_h_TES[t].x for t in self.dt]
        ope_dict['E_h_TES'] = [self.E_h_TES[t].x for t in self.dt]
        return ope_dict


class MESWithMixedTES(MES):
    def __init__(self, *args, radius=2.5, **kwargs):
        super().__init__(*args, **kwargs)
        self.dk = list(range(int(3600/p.delta_k)*len(self.dt)))
        self.F_h_TES = self.m.addVars(self.dt, lb=0, ub=[self._F_h_TES_max(t) for t in self.dt], name='F_h_TES')
        self.P_h_TES = self.m.addVars(self.dt, lb=0, ub=p.P_h_TES_max, name='P_h_TES')
        self.X_TES_out = self.m.addVars(self.dt, lb=0, ub=1, vtype=GRB.BINARY, name='X_TES_out')
        # for i, v in enumerate(self.X_TES_out):
            # self.X_TES_out[v].setAttr('BranchPriority', len(self.dt) - i)
            # self.X_TES_out[v].setAttr('Start', 0)
        self.T_TES = self.m.addVars(self.dk, lb=min(np.array(p.T)[self.dt]), ub=p.T_s, name='T_TES')
        self.radius_TES = radius
        self.V_TES = 2*pi*self.radius_TES**3
        self.result_cls = ResultMixedTES

    def _F_h_TES_max(self, t):
        return p.dot_m_max*p.c_p_kW*(p.T_s - p.T[t])

    def _set_model_constraints(self):
        """Set the constraints of the model with the TES."""
        # TODO: This seems to calculate the temperatures or the power using T_k instead of T_k-1
        dt = self.dt
        dk = self.dk
        A = 6*pi*self.radius_TES**2  # surface area of the tank
        m = 975*self.V_TES  # mass of water for a temperature of 75°C

        self.m.addConstr(self.A_PV + self.A_ST <= 10000, 'Max area of solar panels')
        self.m.addConstrs((self.P_h_CHP[t] <= p.n_th_CHP*(self.F_g_CHP[t] - self.P_e_CHP[t]) for t in dt))
        self.m.addConstrs((self.P_e_CHP[t] <= self.P_CHP_nom for t in dt))
        self.m.addConstrs((self.P_h_GB[t] <= self.P_GB_nom for t in dt))
        self.m.addConstrs((self.P_h_EB[t] <= self.P_EB_nom for t in dt))
        self.m.addConstrs((self.V_e[t] + self.P_e_PV[t] == self.P_e_PV_max(t) for t in dt))
        self.m.addConstrs((self.P_h_ST[t] <= self.P_h_ST_max(t) for t in dt))
        self.elec_balance = self.m.addConstrs(
            (self.P_e_CHP[t] + self.P_e_PV[t] + self.U_e[t]
                - self.P_h_EB[t]/p.n_EB == p.L_e[t] for t in dt),
            'electricity_balance')
        self.chp.add_constraints(self.m)

        self.heat_balance = self.m.addConstrs(
            (self.P_h_CHP[t] + self.P_h_GB[t] + self.P_h_EB[t] + self.P_h_ST[t]
                + self.P_h_TES[t] - self.F_h_TES[t] == p.L_h[t] for t in dt),
            'heat_balance')
        self.m.addConstr(self.T_TES[0] == p.T_r, 'Init TES temperature')
        for k in self.dk[1:]:
            t = dt[int(len(dt)*k/len(self.dk))]
            self.m.addConstr(self.T_TES[k] >= p.T_r*self.X_TES_out[t])
            lh = m*p.c_p_kW*(self.T_TES[k] - self.T_TES[k-1])/p.delta_k
            rh = self.F_h_TES[t] - self.P_h_TES[t] - A*p.K_s*(self.T_TES[k-1] - p.T[t])
            self.m.addConstr(lh == rh)

        # Quadratic constraints, max flow rate
        def phi_start(t): return int((t - dt[0])*len(dk)/len(dt))
        def phi_end(t): return int((t - dt[0] + 1)*len(dk)/len(dt)) - 1
        def k_range(t): return list(range(phi_start(t), phi_end(t) + 1))
        for t in dt:
            const = p.dot_m_max*p.c_p_kW*p.delta_k/3600
            F_max = gp.quicksum(const*(p.T_s - self.T_TES[k]) for k in k_range(t))
            P_max = gp.quicksum(const*(self.T_TES[k] - p.T_r) for k in k_range(t))
            self.m.addConstr(self.F_h_TES[t] <= (1 - self.X_TES_out[t])*F_max)
            self.m.addConstr(self.P_h_TES[t] <= self.X_TES_out[t]*P_max)

        self.m.setParam('NonConvex', 2)  # To solve the non-convex quadratic problem
        self.m.setParam('MIPFocus', 3)  # Select a MIP focus strategy (3 to focus on the bounds)

    def _get_costs(self):
        dt = self.dt
        dt_ratio = len(dt)/8760
        (ATC_sys, ATC_ref) = super()._get_costs()
        ATC_sys += p.C_o_TES_f*volume_to_energy(self.V_TES)*dt_ratio  # TES operation costs
        ATC_sys += p.crf*dt_ratio*p.C_inv_TES*volume_to_energy(self.V_TES)  # TES investment costs
        return ATC_sys, ATC_ref

    def _get_design_dict(self):
        des_dict = super()._get_design_dict()
        des_dict['radius_TES'] = [self.radius_TES]
        des_dict['V_TES'] = [self.V_TES]
        return des_dict

    def _get_operation_dict(self):
        ope_dict = super()._get_operation_dict()
        ope_dict['F_h_TES'] = [self.F_h_TES[t].x for t in self.dt]
        ope_dict['P_h_TES'] = [self.P_h_TES[t].x for t in self.dt]
        ope_dict['X_TES_out'] = [self.X_TES_out[t].x for t in self.dt]
        return ope_dict

    def _get_storage_dict(self):
        dk = self.dk
        # TODO Try to keep keep this consistent with the MH
        m = 975*2*pi*self.radius_TES**3
        return {
            'dk': dk,
            'T_TES': [self.T_TES[k].x for k in dk],
            'E_h_TES': [m*p.c_p_kWh*(self.T_TES[k].x - p.T_r) for k in dk],
        }

    def to_result(self, *args, **kwargs):
        sto_dict = self._get_storage_dict()

        storage = pd.DataFrame(sto_dict)
        storage = storage.set_index('dk')
        result = super().to_result(*args, storage=storage, **kwargs)
        return result


class MESMetaheuristic(MES):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.result_cls = ResultMixedTES
        self.radius = 0
        self.P_h_TES = None
        self.F_h_TES = None
        self.T_TES = None

        self.cost_objective = None
        self.heat_balance = None

    @property
    def V_TES(self):
        return 2*pi*self.radius**3

    def update_constraints(self, radius, P_h, F_h, T):
        self.radius = radius
        self.P_h_TES = P_h
        self.F_h_TES = F_h
        self.T_TES = T
        self._update_heat_balance()

    def _update_heat_balance(self):
        self.m.remove(self.heat_balance)
        c = (self.P_h_CHP[t] + self.P_h_GB[t] + self.P_h_EB[t] + self.P_h_ST[t]
             + self.P_h_TES[t] - self.F_h_TES[t] == p.L_h[t] for t in self.dt)
        self.heat_balance = self.m.addConstrs(c, 'heat_balance')

    def _get_costs(self):
        dt_ratio = len(self.dt)/8760
        (ATC_sys, ATC_ref) = super()._get_costs()
        # TES operation costs
        ATC_sys += p.C_o_TES_f*volume_to_energy(self.V_TES)*dt_ratio
        # TES investment costs
        ATC_sys += p.crf*dt_ratio*p.C_inv_TES*volume_to_energy(self.V_TES)
        return ATC_sys, ATC_ref

    def _get_design_dict(self):
        des_dict = super()._get_design_dict()
        des_dict['radius_TES'] = [self.radius]
        des_dict['V_TES'] = [2*pi*self.radius**3]
        return des_dict

    def _get_storage_dict(self):
        dk = list(range(len(self.T_TES)))
        E_list = []
        m = None
        for k in dk:
            if m is None:
                nodes = len(self.T_TES[k])
                m = 975*2*pi*self.radius**3/nodes
            value = m*p.c_p_kWh*sum(v - p.T_r for v in self.T_TES[k])
            E_list.append(value)
        return {
            'dk': dk,
            'T_TES': [self.T_TES[k] for k in dk],
            'E_h_TES': E_list,
        }

    def _get_operation_dict(self):
        ope_dict = super()._get_operation_dict()
        ope_dict['F_h_TES'] = [self.F_h_TES[t] for t in self.dt]
        ope_dict['P_h_TES'] = [self.P_h_TES[t] for t in self.dt]
        return ope_dict

    def to_result(self, *args, **kwargs):
        sto_dict = self._get_storage_dict()

        storage = pd.DataFrame(sto_dict)
        storage = storage.set_index('dk')
        result = super().to_result(*args, storage=storage, **kwargs)
        return result
