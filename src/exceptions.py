class NonOptimal(Exception):
    def __init__(self, message='No optimal solution found'):
        self.message = message
        super().__init__(self.message)
