import os
# import math
from statistics import mean, stdev
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import enlighten
import re
import time


FR = {
    'Constant efficiency': 'Rendement constant',
    'Triangle linearization': 'Linéarisation triangle',
    'Adapted linearization': 'Linéarisation adaptée',
    'Number of triangles': 'Nombre de triangles',
    'Mean cumulative error': 'Erreur cumulée moyenne',
    'Mean Distance': 'Distance moyenne',
    'Time (s)': 'Durée (s)',
    r'$ATCR$': r'$ATCR$',
    r'$\tau_{RES}$': r'$\tau_{EnR}$',
    r'Solutions $s$ of the Pareto set $\mathcal{S}$': r"Solution $s$ de l'ensemble $\mathcal{S}$",
    r'$\eta_{e,CHP}$': r'$\eta_{e,CHP}$',
    'ST (consumed)': 'ST (utilisé)',
    'ST (not used)': 'ST (inutilisé)',
    'Heat demand': 'Demande de chaleur',
    'PV (used)': 'PV (utilisé)',
    'PV (sold)': 'PV (vendu)',
    'Grid': 'Réseau',
    'Electricity demand': "Demande d'électricité",
    'EB demand': 'Demande EB',
    'Hours (h)': 'Heures (h)',
    'Electricity (kW)': 'Électricité (kW)',
    'Heat (kW)': 'Chaleur (kW)',
    '(heat)': '(chaleur)',
    '(electricity)': '(électricité)',
    'Heat\ndifference (kW)': 'Différence\nchaleur (kW)',
    'Electricity\ndifference (kW)': 'Différence\nélectricité (kW)',
    'Solution $s$': 'Solution $s$',
    'Grid (bought)': 'Réseau (achat)',
    'Grid (sold)': 'Réseau (vente)',
    'CHP (electricity)': 'CHP (électricité)',
    'CHP (heat)': 'CHP (chaleur)',
    'Solver': 'Solveur',
    'Heuristic': 'Heuristique',
    'Heat stored': ' Chaleur stockée',
    'Heat stored (kWh)': ' Chaleur stockée (kWh)',
    'Electricty demand (kW)': 'Demande\nélectricité (kW)',
    'Heat demand (kW)': 'Demande\nchaleur (kW)',
    'Temperature (°C)': 'Température (°C)',
    'Solar irradiation (kW/m²)': 'Irradiation\nsolaire (kW/m²)',
}


lang_dict = None


def set_language(language='en'):
    """Set the language for the text."""
    global lang_dict
    if language == 'fr':
        lang_dict = FR
    elif language == 'en':
        lang_dict = None
    else:
        raise Exception(f'Language not recognized: {language}')


def translate(text):
    """Translate the text using the language selected."""
    if lang_dict is not None:
        try:
            return lang_dict[text]
        except KeyError:
            raise Exception(f'Text "{text}" not found in dictionary')
    else:
        return text


def sorted_alphanumeric(data):
    """
    Sort a list of strings by alpha-numeric values.

    https://stackoverflow.com/questions/4813061/non-alphanumeric-list-order-from-os-listdir/48030307#48030307
    """
    def convert_text(text): return int(text) if text.isdigit() else text
    def alphanum_key(key): return [convert_text(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


class pbar_manager:
    counter = 0
    manager = None
    status = None
    lock = False

    def __init__(self, status=None, lock=False):
        if pbar_manager.status is None:
            pbar_manager.status = status
        if pbar_manager.counter == 0:
            pbar_manager.lock = lock

    def __enter__(self):
        if pbar_manager.manager is None:
            pbar_manager.manager = enlighten.get_manager()
            if pbar_manager.status is not None:
                pbar_manager.status = pbar_manager.manager.status_bar(
                    status_format=u'{status}{fill}{elapsed}',
                    color='bold_underline_bright_white_on_lightslategray',
                    status=pbar_manager.status,
                    justify=enlighten.Justify.CENTER, autorefresh=True, min_delta=0.5)
        pbar_manager.counter += 1
        if pbar_manager.counter == 1 or not pbar_manager.lock:
            return pbar_manager.manager
        else:
            return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        pbar_manager.counter -= 1
        if pbar_manager.counter <= 0:
            pbar_manager.manager.stop()
            pbar_manager.manager = None
            pbar_manager.status = None
            pbar_manager.lock = False

    @classmethod
    def exists(cls):
        return cls.manager is not None


class counter:
    def __init__(self, *args, title=None, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.counter = None
        self.title = title
        self.nbr = None
        self.start = None
        self.manager = None

    def __enter__(self):
        self.manager = pbar_manager(self.title).__enter__()
        if self.manager is not None:
            self.counter = self.manager.counter(*self.args, **self.kwargs)
        self.nbr = 0
        self.start = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.counter is not None:
            self.counter.close()

    def update(self, *args, incr=1, **kwargs):
        self.nbr += incr
        if self.counter is not None:
            self.counter.update(*args, **kwargs)

    @property
    def count(self):
        if self.counter is not None:
            return self.counter.count
        else:
            return self.nbr

    @property
    def elapsed(self):
        if self.counter is not None:
            return self.counter.elapsed
        else:
            return time.time() - self.start


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class colors_accent:
    _colors = plt.get_cmap('Set1').colors
    CHP = _colors[1]
    GB = _colors[0]
    EB = _colors[4]
    ST = _colors[3]
    ST2 = _colors[8]
    GRID = _colors[2]
    PV = _colors[5]
    PV2 = _colors[7]
    TES = 'tab:brown'


class colors_gray:
    _colors = plt.get_cmap('Paired').colors
    CHP = _colors[1]
    GB = _colors[7]
    EB = _colors[10]
    ST = _colors[5]
    ST2 = _colors[4]
    GRID = _colors[9]
    PV = _colors[3]
    PV2 = _colors[2]
    TES = _colors[11]


colors = colors_accent


class hatches:
    CHP = '//'
    GB = '\\\\'
    EB = 'xx'
    ST = '++'
    ST2 = '--'
    GRID = '..'
    PV = 'OO'
    PV2 = 'oo'
    TES = '**'


PRECISION = 1e-5


def same(values):
    """Check that all the values of the list are the same."""
    return len(set(values)) == 1


def hl_text(text, color=None, f=''):
    if color is None:
        return f'{text:{f}}'
    else:
        return f'{color}{text:{f}}{bcolors.ENDC}'


def same_values(values, precision=0):
    """Check if all the values are the same, with a precision.

    Args:
        values: iterable of the values
        precision: precision required to consider that the values are the same (default: 0)
    """
    difference = (abs(v - values[0]) for v in values)
    return all((d < precision for d in difference))


def pretty_unit_energy(value, ratio=1000):
    """Return a string with the energy in (k|M̀|G)Wh.

    Args:
        value: value in Wh
        ratio: the ratio of the unit (e.g. 1000 if value is in kWh)
    """
    v = value*ratio
    if v >= 10**9:
        return f'{v/10**9:.2f}', 'GWh'
    if v >= 10**6:
        return f'{v/10**6:.1f}', 'MWh'
    if v >= 10**3:
        return f'{v/10**3:.1f}', 'kWh'
    else:
        return f'{v:.1f}', 'Wh'


def get_triangles_difference(before, after):
    if before is None or after is None:
        return None, 'No triangles in the solution'
    if len(before) != len(after):
        return None, 'Not the same number of triangles'
    text = []
    for i, (b, a) in enumerate(zip(before, after)):
        if b != a:
            text.append('({}) {} -> {}'.format(i, b, a))
    if len(text) > 0:
        return True, ', '.join(text)
    else:
        return False, 'No difference'


def get_save_path(start, duration, N, number, prefix='', directory=''):
    """Return the path name for this experiment."""
    suffix = '{}+{}_{}_{}'.format(start, duration, N, number)
    fname = prefix + suffix + '.pkl.gz'
    path = os.path.join(directory, fname)
    return path


def print_energy_balance(*results, labels=['Optimal', 'Heuristic']):
    """Print the energy balance with the differences highlighted.

    Args:
        ro: the optimal result
        ra: the approximated result
    """
    # TODO: Change to be able to display more than two results
    min_len = max((len(lab) for lab in labels))
    title = f'{"":<{min_len}} |'
    lines = [f'{lab:<{min_len}} |' for lab in labels]
    heats = {
        'CHP': [sum(r.operation['P_h_CHP']) for r in results],
        'GB': [sum(r.operation['P_h_GB']) for r in results],
        'EB': [sum(r.operation['P_h_EB']) for r in results],
        'ST': [sum(r.operation['P_h_ST']) for r in results],
    }
    elecs = {
        'CHP': [sum(r.operation['P_e_CHP']) for r in results],
        'PV': [sum(r.operation['P_e_PV']) for r in results],
        'U': [sum(r.operation['U_e']) for r in results],
    }
    objectives = {
        'ACR': [r.ACR for r in results],
        'RES': [r.RES for r in results],
    }
    for di, d in enumerate((elecs, heats, objectives)):
        for k, values in d.items():
            if di != 2:
                val_print = [' '.join(pretty_unit_energy(v)) for v in values]
            else:
                val_print = [f'{v:.2f}' for v in values]
            s = max(len(k), max([len(v) for v in val_print]))
            title += f' {k:^{s}} '
            if not same_values(values, precision=1):
                color = bcolors.WARNING
            else:
                color = None
            for i, v in enumerate(val_print):
                text = hl_text(v, color, f'^{s}')
                lines[i] += f' {text} '
        title += '|'
        for i, _ in enumerate(lines):
            lines[i] += '|'
    print(title)
    for line in lines:
        print(line)


def print_energy_balance_latex(*results, labels=['Optimal', 'Heuristic'], diff=True):
    """Print the energy balance with the differences highlighted.

    Args:
        ro: the optimal result
        ra: the approximated result
    """
    assert same([r.dt[0] for r in results]), 'The results do not have the same start time'
    assert same([len(r.dt) for r in results]), 'The two results do not have the same duration'

    # TODO: Change to be able to display more than two results
    lines = [[f'{lab}'] for lab in labels]
    heats = {
        'CHP': [sum(r.operation['P_h_CHP']) for r in results],
        'GB': [sum(r.operation['P_h_GB']) for r in results],
        'EB': [sum(r.operation['P_h_EB']) for r in results],
        'ST': [sum(r.operation['P_h_ST']) for r in results],
    }
    elecs = {
        'CHP': [sum(r.operation['P_e_CHP']) for r in results],
        'PV': [sum(r.operation['P_e_PV']) for r in results],
        'U': [sum(r.operation['U_e']) for r in results],
    }
    # objectives = {
        # 'ACR': [r.ACR for r in results],
        # 'RES': [r.RES for r in results],
    # }
    for di, d in enumerate((elecs, heats)):
        for k, values in d.items():
            if di != 2:
                val_print = []
                for v in values:
                    value, unit = pretty_unit_energy(v)
                    val_print.append('\\SI{' + value + '}{' + unit + '}')
            s = max(len(k), max([len(v) for v in val_print]))
            if not same_values(values, precision=1) and diff:
                color = bcolors.WARNING
            else:
                color = None
            for i, v in enumerate(val_print):
                if color:
                    v = '\\Diff{' + v + '}'
                text = hl_text(v, color, f'^{s}')
                lines[i].append(f'{text}')
    lines_str = [' & '.join(line) for line in lines]
    for i, _ in enumerate(lines_str):
        lines_str[i] += ' \\\\'
    for line in lines_str:
        print(line)


def plot_pareto(*results, legend=None):
    """Plot Pareto fronts.

    Args:
        *results: the results to plot
        legend: the legend
    """
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    default_figsize = plt.rcParams['figure.figsize']
    figsize = 0.9*default_figsize[0], 0.7*default_figsize[1]
    fig = plt.figure(figsize=figsize)
    ax = plt.subplot(111)
    styles = ['o-', '.--']
    for i, r in enumerate(results):
        r.plot_pareto(ax, color=colors[i], style=styles[i])
    if legend is not None:
        ax.legend(legend)
    return fig


def plot_result(*results, legend=None):
    """Plot results."""
    assert same([r.dt[0] for r in results]), 'The results do not have the same start time'
    assert same([len(r.dt) for r in results]), 'The two results do not have the same duration'

    default_figsize = plt.rcParams['figure.figsize']
    figsize = 0.5*1.5*default_figsize[0], len(results)*0.5*default_figsize[1]

    fig, axs = plt.subplots(nrows=len(results), ncols=1, sharex=True, sharey='col', squeeze=False, figsize=figsize)
    for i, r in enumerate(results):
        kwargs = {
            # 'hide_xlabel': i == 0,
            'legend': i == len(results) - 1,
        }
        title = legend[i] if legend is not None else f'{i+1}/{len(results)}'
        ax_heat = axs[i, 0]
        ax_elec = axs[i, 1]
        ax_heat.set_title(f'{title} {translate("(heat)")}')
        r._plot_heat(ax_heat, colors=True, **kwargs)
        ax_elec.set_title(f'{title} {translate("(electricity)")}')
        r._plot_elec(ax_elec, colors=True, **kwargs)
    return fig


def plot_bisector(result1, result2, labels=None):
    """Plot bisector figure of two results."""
    assert result1.dt[0] == result2.dt[0], 'The results do not have the same start time'
    assert len(result1.dt) == len(result2.dt), 'The two results do not have the same duration'

    default_figsize = plt.rcParams['figure.figsize']
    figsize = 2*default_figsize[1], default_figsize[1]

    fig = plt.figure(figsize=figsize)
    # Heat
    ax = plt.subplot(121)
    ax.set_aspect('equal', adjustable='box')
    plt.grid()
    ax.plot(list(result1.operation['P_h_CHP']), list(result2.operation['P_h_CHP']), '.', color=colors.CHP, label='CHP')
    ax.plot(list(result1.operation['P_h_GB']), list(result2.operation['P_h_GB']), '.', color=colors.GB, label='GB')
    ax.plot(list(result1.operation['P_h_EB']), list(result2.operation['P_h_EB']), '.', color=colors.EB, label='EB')
    ax.plot(list(result1.operation['P_h_ST']), list(result2.operation['P_h_ST']), '.', color=colors.ST, label='ST')
    if labels is not None:
        ax.set_xlabel(labels[0])
        ax.set_ylabel(labels[1])
    ax.set_title('Heat')
    ax.legend()

    # Electricity
    ax = plt.subplot(122)
    ax.set_aspect('equal', adjustable='box')
    plt.grid()
    ax.plot(list(result1.operation['P_e_CHP']), list(result2.operation['P_e_CHP']), '.', color=colors.CHP, label='CHP')
    ax.plot(list(result1.operation['P_e_PV']), list(result2.operation['P_e_PV']), '.', color=colors.PV, label='PV')
    ax.plot(list(result1.operation['U_e']), list(result2.operation['U_e']), '.', color=colors.GRID, label='Grid')
    ax.plot(list(result1.operation['V_e']), list(result2.operation['V_e']), '.', color=colors.PV2, label='PV (Sold)')
    if labels is not None:
        ax.set_xlabel(labels[0])
        ax.set_ylabel(labels[1])
    ax.set_title('Electricity')
    ax.legend()
    return fig


def display_capacity_factor(*results, labels=None):
    assert same([r.dt[0] for r in results]), 'The results do not have the same start time'
    assert same([len(r.dt) for r in results]), 'The two results do not have the same duration'

    rs = results

    d = {
        'CHP (heat)': [sum(r.operation['P_h_CHP']) for r in rs],
        'GB': [sum(r.operation['P_h_GB']) for r in rs],
        'EB': [sum(r.operation['P_h_EB']) for r in rs],
        'ST': [sum(r.operation['P_h_ST']) for r in rs],
        'CHP (elec)': [sum(r.operation['P_e_CHP']) for r in rs],
        'PV': [sum(r.operation['P_e_PV']) for r in rs],
    }
    energy = pd.DataFrame(data=d)
    d = {
        'GB': [r.design['Q_GB_nom'][0] for r in rs],
        'EB': [r.design['Q_EB_nom'][0] for r in rs],
        'ST': [r.design['A_ST'][0] for r in rs],
        'CHP (elec)': [r.design['P_CHP_nom'][0] for r in rs],
        'PV': [r.design['A_PV'][0] for r in rs],
    }
    capacity = pd.DataFrame(data=d)
    length = len(rs[0].dt)
    d = {
        'CHP (heat)': length*capacity['CHP (elec)']/(0.8*0.7),
        'GB': length*capacity['GB'],
        'EB': length*capacity['EB'],
        'ST': [sum(r.P_h_ST_max(r.design['A_ST'][0], t) for t in r.dt) for r in rs],
        'CHP (elec)': length*capacity['CHP (elec)'],
        'PV': [sum(r.P_e_PV_max(r.design['A_PV'][0], t) for t in r.dt) for r in rs],
    }
    total = pd.DataFrame(data=d)
    if labels is not None:
        print(f'{" ":<10}  | ', end='')
        for label in labels:
            print(f'{label:^20}', end=' | ')
        print('')
    for techno in ('CHP (heat)', 'GB', 'EB', 'CHP (elec)', 'ST', 'PV'):
        ratio = energy[techno]/total[techno]
        print(f'{techno:<10} ', end=' | ')
        for k in ratio.index:
            finite = np.isfinite(ratio[k]).all()
            if len(rs) > 1 and finite and not same_values(ratio, PRECISION):
                ratio_color = bcolors.WARNING
            else:
                ratio_color = None
            if finite:
                ratio_text = hl_text(100*ratio[k], ratio_color, '>5.1f')
            else:
                ratio_text = '  ---'
            if techno == 'CHP (heat)':
                pretty_capacity = None
            else:
                if len(rs) > 1 and not same_values(capacity[techno], 0.01):
                    color = bcolors.WARNING
                else:
                    color = None
                value = hl_text(capacity[techno][k], color, '>8.0f')
                unit = 'm²' if techno in ('ST', 'PV') else 'kW'
                pretty_capacity = f'{value} {unit}'
            if pretty_capacity is None:
                print(f'{ratio_text} %             ', end=' | ')
            else:
                print(f'{ratio_text} %  {pretty_capacity}', end=' | ')
        print('')


def plot_all_boxplots(optimal, heuristic):
    assert len(optimal.results) == len(heuristic.results), 'The two results do not have the same number of solutions'
    assert optimal.results[0].dt[0] == heuristic.results[0].dt[0], 'The two results do not have the same start time'
    assert len(optimal.results[0].dt) == len(heuristic.results[0].dt), 'The two results do not have the same duration'

    keys_list = [
        ['P_h_CHP', 'P_h_GB', 'P_h_EB', 'P_h_ST'],
        ['P_e_CHP', 'P_e_PV', 'U_e', 'V_e']
    ]
    design_list = [
        ['P_CHP_nom', 'Q_GB_nom', 'Q_EB_nom', 'A_ST'],
        ['P_CHP_nom', 'A_PV', None, None]
    ]
    titles_list = [
        [translate('CHP (heat)'), 'GB', 'EB', 'ST'],
        [translate('CHP (electricity)'), 'PV', translate('Grid (bought)'), translate('Grid (sold)')],
    ]
    default_figsize = plt.rcParams['figure.figsize']
    # figsize = 2*default_figsize[0], 1*default_figsize[1]
    figsize = 1.4*default_figsize[0], 0.7*default_figsize[1]
    fig, axs = plt.subplots(nrows=2, ncols=4, figsize=figsize, sharey='row', sharex='all')
    for v in range(2):
        keys = keys_list[v]
        titles = titles_list[v]
        for i, key in enumerate(keys):
            values_dict = {}
            for s, (ro, rh) in enumerate(zip(optimal.results, heuristic.results)):
                design_key = design_list[v][i]
                if design_key is None or ro.design[design_key][0] > PRECISION:
                    values = rh.operation[key] - ro.operation[key]
                    values = values.round(3)  # Round to remove really small values
                else:
                    values = pd.Series(float('nan'), index=ro.operation[key].index)
                values_dict[s + 1] = values
            df = pd.DataFrame(values_dict)
            sns.boxplot(data=df, ax=axs[v, i], whis=[5, 95])
            axs[v, i].set_title(titles[i])
            axs[v, i].grid(True, alpha=0.5)
    axs[0, 0].locator_params(axis='y', steps=[1, 2, 4, 5, 10])
    axs[1, 0].locator_params(axis='y', steps=[1, 2, 3, 4, 5, 10])
    axs[0, 0].set_ylabel(translate('Heat\ndifference (kW)'))
    axs[1, 0].set_ylabel(translate('Electricity\ndifference (kW)'))
    for ax in axs[1, :]:
        ax.set_xlabel(translate('Solution $s$'))
    return fig


def _print_global_metrics(reference, other):
    # cum_distance = []
    cum_ACR, cum_RES = [], []
    for i, (r1, r2) in enumerate(zip(reference.results, other.results)):
        # cum_distance.append(math.sqrt(r1.ACR**2 + r1.RES**2))
        # cum_distance[-1] -= math.sqrt(r2.ACR**2 + r2.RES**2)
        cum_ACR.append(abs(r2.ACR - r1.ACR)/abs(r1.ACR))
        cum_RES.append(abs(r2.RES - r1.RES)/abs(r1.RES))

    error = cum_ACR + cum_RES
    print(f'Error ACR:  mean: {mean(cum_ACR):.3e}, std: {stdev(cum_ACR):.3e}, max: {max(cum_ACR):.3e}')
    print(f'Error RES:  mean: {mean(cum_RES):.3e}, std: {stdev(cum_RES):.3e}, max: {max(cum_RES):.3e}')
    print(f'All errors:  mean: {mean(error):.2e}, std: {stdev(error):.2e}, max: {max(abs(e) for e in error):.2e}')
    # print(f'Cumulative distance:  mean: {mean(cum_distance):.3e}, std: {stdev(cum_distance):.3e}')


def _print_time_improvement(reference, other):
    # print('Optimal total duration: {:.3f}s'.format(optimal.duration))
    # print('Approximate total duration: {:.3f}s'.format(heuristic.duration))
    # print('Total improvement: {:.2f}%'.format(abs((heuristic.duration - optimal.duration)/optimal.duration)*100))
    d1 = reference.duration
    d2 = other.duration
    improvement = 100*abs(other.duration - reference.duration)/reference.duration
    print(f'Optimal: {d1:.2f}s, Approximate: {d2:.2f}s ({improvement:.2f}% improvement)')


def show_comparison(optimal, heuristic, verbosity=0):
    assert len(optimal.results) == len(heuristic.results), 'The two results do not have the same number of solutions'
    assert optimal.results[0].dt[0] == heuristic.results[0].dt[0], 'The two results do not have the same start time'
    assert len(optimal.results[0].dt) == len(heuristic.results[0].dt), 'The two results do not have the same duration'

    print(f'{bcolors.HEADER}###### Results ######{bcolors.ENDC}')

    _print_time_improvement(optimal, heuristic)
    _print_global_metrics(optimal, heuristic)

    if verbosity:
        print('')
        for i, (ro, ra) in enumerate(zip(optimal.results, heuristic.results)):
            print(f'{bcolors.UNDERLINE}Solution {i+1}/{len(optimal.results)}{bcolors.ENDC}')
            _print_time_improvement(ro, ra)
            so = ro.kwargs.get('solution')
            sa = ra.kwargs.get('solution')
            diff, text = get_triangles_difference(so, sa)
            if diff is None:
                print(f'{bcolors.FAIL}{text}{bcolors.ENDC}')
            elif diff is False:
                print(f'{bcolors.OKGREEN}{text}{bcolors.ENDC} ACR: {ro.ACR:>7.4f}   RES: {ro.RES:>7.4f}')
            if diff or diff is None:
                print(f'{"Optimal":<13} ACR: {ro.ACR:>7.4f}   RES: {ro.RES:>7.4f}')
                print(f'{"Approximate":<13} ACR: {ra.ACR:>7.4f}   RES: {ra.RES:>7.4f}')
                # error_ACR = abs(ra.ACR - ro.ACR)/abs(ro.ACR)
                # error_RES = abs(ra.RES - ro.RES)/abs(ro.RES)
                # print(f'{"Error":<13} ACR: {100*error_ACR:>6.3e}%   RES: {100*error_RES:>6.3e}%')
                # print(f'{"Error":<13} ACR: {error_ACR:>7.2e}  RES: {error_RES:>7.2e}')
                error_ACR = ((ra.ACR - ro.ACR)/ro.ACR)
                error_RES = ((ra.RES - ro.RES)/ro.RES)
                print(f'{"Error":<13} ACR: {100*error_ACR:>6.2f}%   RES: {100*error_RES:>6.2f}%')
                if verbosity > 3:
                    print(f'\n    {bcolors.UNDERLINE}Differences in the triangles{bcolors.ENDC}')
                    print('(t) optim. -> approx.:', text)
            if verbosity > 1:
                print(f'\n    {bcolors.UNDERLINE}Energy balance{bcolors.ENDC}')
                print_energy_balance(ro, ra)
            if verbosity > 2:
                print(f'\n    {bcolors.UNDERLINE}Capacity and statistics on the operation{bcolors.ENDC}')
                print(f'        {bcolors.UNDERLINE}Optimal{bcolors.ENDC}')
                ro.display(objectives=False)
                print(f'        {bcolors.UNDERLINE}Heuristic{bcolors.ENDC}')
                ra.display(objectives=False)
            yield ro, ra
            print('')
