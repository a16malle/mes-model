from math import pi
import random
from scipy.ndimage import gaussian_filter1d
import numpy as np
import logging
from functools import lru_cache

from . import parameters as p
from .utils import bcolors as ts, counter
from .exceptions import NonOptimal


logger = logging.getLogger(__name__)


def trunc_gaussian(mu, sigma, low=-p.dot_m_max, high=p.dot_m_max):
    """Return value from truncated gaussian distribution.

    Uses a recursive call to sample again if the value is lower or hight than
    the limits.

    Args:
        mu: mean or location
        sigma: variance or squared scale
        low: low bound
        high: high bound
    """
    v = random.gauss(mu, sigma)
    if v < low or v > high:
        v = trunc_gaussian(mu, sigma, low, high)
    return v


@lru_cache(maxsize=1024)
def surface_area(radius, node, nodes_total):
    """Calculate the surface area of the node and cache the result."""
    r = radius
    h = 2*r
    A_s = 2*pi*r*h/nodes_total  # Divide by the nbr of nodes
    if node == 0:
        A_s += section_area(radius)
    if node == nodes_total - 1:
        A_s += section_area(radius)
    return A_s


def section_area(radius):
    return pi*radius**2


@lru_cache(maxsize=256)
def mass(radius, nodes_total):
    """Calculate the mass of a node and cache the result."""
    V = 2*pi*radius**3  # Volume of storage
    m = 975*V  # Density of water at 75°C
    m = m/nodes_total  # Divide by the nbr of nodes
    return m


class Individual:
    def __init__(self, environment, radius=2.5, flow_rate=[]):
        self.environment = environment
        self._fitness = None
        self._flow_rate = []
        self._radius = None
        self._temperature = None
        self._P_out = None
        self._F_in = None
        self.radius = radius
        self.flow_rate = flow_rate

    @property
    def tes(self):
        return self.environment.solver.mes.tes

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, value):
        if not p.radius_min <= value <= p.radius_max:
            raise ValueError(f'Incorrect TES radius, {value} given')
        value = 2.5
        different = value != self._radius
        self._radius = value
        if len(self.flow_rate) > 0 and different:
            # If the flow rate is set and the radius if different,
            # repair the flow rate
            self.flow_rate = self.flow_rate

    @property
    def mass(self):
        return mass(self.radius, self.environment.nodes)

    def surface_area(self, node):
        """
        Surface area of the node.

        We know that h = 2*r to minimize the heat losses.

        Args:
            node: index of the node, starting from 1
        """
        return surface_area(self.radius, node, self.environment.nodes)

    @property
    def flow_rate(self):
        return self._flow_rate

    @flow_rate.setter
    def flow_rate(self, value):
        if not all(-p.dot_m_max <= v <= p.dot_m_max for v in value):
            raise ValueError('Mass flow rate too large')
        flow_rate = tuple(self._repair_flow_rate(value))
        if flow_rate != self._flow_rate:
            # If the flow rate is different reset the fitness value
            self._fitness = None
        self._flow_rate = flow_rate

    @property
    def fitness(self):
        if self._fitness is None:
            self._fitness = self.environment.calculate_fitness(self)
        return self._fitness

    @property
    def temperature(self):
        return self._temperature

    @property
    def P_out(self):
        return self._P_out

    @property
    def F_in(self):
        return self._F_in

    @property
    def sigma_f(self):
        return self.environment.sigma_factor

    def __eq__(self, other):
        return (self._radius == other._radius
                and self._flow_rate == other._flow_rate)

    def _get_flow(self, flow_rate):
        if flow_rate > 0:
            fr_in = flow_rate
            fr_out = 0
        else:
            fr_in = 0
            fr_out = -flow_rate
        return fr_in, fr_out

    def _check_repair_flow_rate_timestep(self, t, flow_rate, T_previous):
        """
        Calculate the temperature at the time step and repair the flow
        rate if necessary.
        """
        # TODO: Move to another class or function
        T = {i: v for i, v in enumerate(T_previous)}
        T[-1] = p.T_s
        T[self.environment.nodes] = p.T_r
        temperatures = []
        k_step = int(3600/p.delta_k)
        if abs(flow_rate) < 1e-2:
            flow_rate = 0
        fr_in, fr_out = self._get_flow(flow_rate)
        if T[0] < p.T_r and fr_out > 0:
            flow_rate = 0
            fr_out = 0
        A_c = section_area(self.radius)
        Z_delta = 2*self.radius/self.environment.nodes
        ratio_cond = p.k_w*A_c/Z_delta
        ratio_temp = p.delta_k/(self.mass*p.c_p_kW)
        P_out = 0
        F_in = 0
        for k in range(k_step):
            T_next = T.copy()
            P = fr_out*p.c_p_kW*(T[0] - p.T_r)
            P_out += P/k_step
            F_in += fr_in*p.c_p_kW*(p.T_s - T[self.environment.nodes - 1])/k_step
            temperatures.append([])
            for n in range(self.environment.nodes):
                flow = fr_in*p.c_p_kW*(T[n-1] - T[n]) - fr_out*p.c_p_kW*(T[n] - T[n+1])
                losses = self.surface_area(n)*p.K_s*(T[n] - p.T[t])
                # Distance between center of two nodes: height divided by nbr nodes
                cond = 0
                if n > 0:
                    cond += ratio_cond*(T[n] - T[n-1])
                if n < self.environment.nodes - 1:
                    cond += ratio_cond*(T[n] - T[n+1])
                T_next[n] = ratio_temp*(flow - cond - losses) + T[n]
                temperatures[-1].append(T_next[n])
            T = T_next
            if T[0] < p.T_r and fr_out > 0 or P > p.L_h[t]:
                logger.debug(
                    f'Repairing wrong flow rate: {flow_rate:.2g}, '
                    'temperatures: {T:.1f}, P: {P:.1f}, L_h: {p.L_h[t]:.1f}')
                flow_rate = 0.9*flow_rate
                return self._check_repair_flow_rate_timestep(t, flow_rate, T_previous)
        return flow_rate, temperatures, P_out, F_in

    def _repair_flow_rate(self, values):
        """Repair and return the flow rate."""
        # TODO: Repair only the parts of the flow rate that changed if the radius is the same
        flow_rate = list(values)
        temperature = []
        P_out = []
        F_in = []
        last_T_TES = [p.T_r]*self.environment.nodes
        for it, t in enumerate(self.environment.dt):
            fr, temp, P, F = self._check_repair_flow_rate_timestep(t, flow_rate[it], last_T_TES)
            flow_rate[it] = fr
            temperature.extend(temp)
            P_out.append(P)
            F_in.append(F)
            last_T_TES = temperature[-1]
        self._temperature = temperature
        self._P_out = P_out
        self._F_in = F_in
        return flow_rate

    def get_parts_flow_rate(self):
        """
        Return the parts with a flow rate.

        Returns the start and end index of parts where the flow rate increases or decreases continuously.
        """
        parts = []
        start = None
        prev = 0
        for i, v in enumerate(self.flow_rate):
            if v > 0 and prev <= 0 or v < 0 and prev >= 0:
                if start is None:
                    start = i
                else:
                    parts.append((start, i - 1))
                    start = i
            elif v == 0 and start is not None:
                parts.append((start, i - 1))
                start = None
            prev = v
        return parts

    def mutation_extend(self):
        """Extend of shrink part of the flow_rate."""
        parts = self.get_parts_flow_rate()
        if len(parts) == 0:
            return self
        i1, i2 = random.choice(parts)
        flow_rate = list(self.flow_rate)
        if random.random() < 0.5:
            flow_rate[i1] = 0
            flow_rate[i2] = 0
        else:
            if flow_rate[i1] > 0:
                vmin, vmax = 0, p.dot_m_max
            else:
                vmin, vmax = -p.dot_m_max, 0
            if i1 > 0:
                flow_rate[i1 - 1] = random.uniform(vmin, vmax)
            if i2 < len(flow_rate):
                flow_rate[i2 + 1] = random.uniform(vmin, vmax)
        return Individual(self.environment, self.radius, flow_rate)

    def mutation_change(self):
        """Change a part of the flow rate by a random ratio."""
        parts = self.get_parts_flow_rate()
        if len(parts) == 0:
            return self
        i1, i2 = random.choice(parts)
        flow_rate = list(self.flow_rate)
        for i in range(i1, i2 + 1):
            flow_rate[i] = trunc_gaussian(flow_rate[i], self.sigma_f*2*p.dot_m_max/6)
        return Individual(self.environment, self.radius, flow_rate)

    def mutation_shift(self):
        """Shift a part of the flow rate."""
        parts = self.get_parts_flow_rate()
        if len(parts) == 0:
            return self
        i1, i2 = random.choice(parts)
        shift = random.choice((-2, -1, 1, 2))
        fr = list(self.flow_rate)
        if shift > 0 and i2 + shift + 1 < len(fr):
            flow_rate = fr[:i1] + [0]*shift + fr[i1:i2+1] + fr[i2+shift+1:]
            assert len(flow_rate) == len(self.flow_rate), \
                f'Different lengths after positive shift: {len(self.flow_rate)} -> {len(flow_rate)}'
        elif shift < 0 and i1 + shift >= 0:
            s = -shift
            flow_rate = fr[:i1-s] + fr[i1:i2+1] + [0]*s + fr[i2+1:]
            assert len(flow_rate) == len(self.flow_rate), \
                f'Different lengths after negative shift: {len(self.flow_rate)} -> {len(flow_rate)}'
        else:
            flow_rate = self.flow_rate
        return Individual(self.environment, self.radius, flow_rate)

    def mutation_random(self, chance=0.1):
        """Gaussian convolution mutation."""
        flow_rate = list(self.flow_rate)
        for i, fr in enumerate(flow_rate):
            if random.random() < chance:
                flow_rate[i] = trunc_gaussian(flow_rate[i], self.sigma_f*2*p.dot_m_max/6)
        return Individual(self.environment, self.radius, flow_rate)

    def mutation_shift_all(self):
        """Shift all the time series."""
        if random.random() < 0.5:
            flow_rate = [0] + list(self.flow_rate[:-1])
        else:
            flow_rate = list(self.flow_rate[1:]) + [0]
        return Individual(self.environment, self.radius, flow_rate)

    def mutation_radius(self):
        radius = trunc_gaussian(
            self.radius, self.sigma_f*(p.radius_max - p.radius_min)/6,
            p.radius_min, p.radius_max)
        return Individual(self.environment, radius, self.flow_rate)

    def mutate(self):
        choice = random.choices((1, 2, 3, 4, 5))[0]
        if choice == 1:
            sol = self.mutation_change()
        elif choice == 2:
            sol = self.mutation_shift()
        elif choice == 3:
            sol = self.mutation_extend()
        elif choice == 4:
            sol = self.mutation_random()
        elif choice == 5:
            sol = self.mutation_radius()
        return sol


class GeneticAlgorithm:
    def __init__(self, solver, population=[], nbr_iterations=100,
                 nbr_no_improvement=None, nodes=1,
                 crossover_weights=(0.67, 0.33), sigma_factor=1,
                 elite_percent=0.2, seed=None):
        # TODO: Allow to set a fixed radius
        self.solver = solver
        self.population = population
        self.size = len(self.population)
        self.iteration = 0
        self.nbr_iterations = nbr_iterations
        self.nbr_no_improvement = nbr_no_improvement
        self.total_time = 0
        self.seed = seed
        self.gap_improvement = 0
        self.improvement_nbr = [0, 0]
        self.nodes = nodes
        self.crossover_weights = crossover_weights
        self.sigma_factor = sigma_factor
        self.elite_percent = elite_percent

    @property
    def seed(self):
        return self._seed

    @seed.setter
    def seed(self, seed):
        self._seed = seed
        random.seed(seed)

    @property
    def dt(self):
        return self.solver.mes.dt

    def calculate_fitness(self, solution):
        # TODO: Keep the results of the best solution
        try:
            values = self.solver.calculate_objective_functions(solution)
        except NonOptimal as e:
            # Overwrite with a new random solution if not feasible
            logging.warning(str(e))
            new = self.create_random_individual()
            solution.radius = new.radius
            solution.flow_rate = new.flow_rate
            return solution.fitness
        return (1 - 1e-2)*values[0] + 1e-2*values[1]

    def create_random_individual(self):
        """Create a random solution."""
        radius = random.uniform(p.radius_min, p.radius_max)
        loc = 0
        scale = 2*p.dot_m_max/(6*2)
        flow_rate = [trunc_gaussian(loc, scale) for t in self.dt]
        return Individual(self, radius=radius, flow_rate=flow_rate)

    def create_from_solar_radiation(self):
        """Create a solution following the solar radiation."""
        G_t = np.array([p.G[t] for t in self.dt])
        derivative = gaussian_filter1d(G_t, sigma=1, order=1)
        # Multiply by 0.99 to avoid float precision error giving a value too high
        scale = 0.99*p.dot_m_max/abs(derivative).max()
        flow_rate = scale*derivative
        radius = random.uniform(p.radius_min, p.radius_max)
        sol = Individual(self, radius=radius, flow_rate=flow_rate)
        sol = sol.mutate()
        return sol

    def init_random_population(self, size):
        logger.info('Initializing population...')
        population = []
        while len(population) < size:
            population.append(self.create_from_solar_radiation())
            population.append(self.create_random_individual())
        self.population = population
        self.size = size
        logger.info('Initialization done')

    def select_tournament(self, size=2):
        """Return a solution based on a tournament selection."""
        individuals = random.sample(self.population, size)
        return max(individuals, key=lambda s: s.fitness)

    def select_roulette(self):
        """Return a solution based on a roulette wheel selection."""
        best = max(self.population, key=lambda s: s.fitness)
        selected = None
        while selected is None:
            individual = random.choice(self.population)
            if random.random() < individual.fitness/best.fitness:
                selected = individual
        return selected

    def select_rank(self):
        ordered = self.get_ordered(best_first=False)
        weights = []
        for i in range(len(ordered)):
            weights.append((i+1)/len(ordered))
        individuals = random.choices(ordered, weights)
        return individuals[0]

    def crossover(self, sol1, sol2):
        flow_rate = []
        w1, w2 = self.crossover_weights
        if sol1.fitness > sol2.fitness:
            r1, r2 = w1, w2
        else:
            r1, r2 = w2, w1
        for v1, v2 in zip(sol1.flow_rate, sol2.flow_rate):
            value = r1*v1 + r2*v2
            flow_rate.append(value)
        radius = r1*sol1.radius + r2*sol2.radius
        return Individual(self, radius, flow_rate)

    def get_ordered(self, best_first=True):
        # Display a progress bar of the fitness calculation
        with counter(total=len(self.population), desc='Fitness', leave=False) as pbar:
            for s in self.population:
                s.fitness
                pbar.update()
        ordered = sorted(
            self.population, key=lambda s: s.fitness, reverse=best_first)
        return ordered

    def solve(self):
        prev_best = None
        with counter(title='Genetic Algorithm', total=self.nbr_iterations,
                     desc='Iterations', leave=False) as pbar:
            # Calculate fitness before first iteration
            self.get_ordered()
            for i in self.iterate():
                ordered = self.get_ordered()
                best = ordered[0]
                if prev_best is None or best.fitness > prev_best.fitness:
                    c = ts.OKCYAN
                    prev_best = best
                else:
                    c = ts.WARNING
                logger.info(
                    f'{c}Iter. {i+1}{ts.ENDC} '
                    + ' '.join([f'{s.fitness:.2f}' for s in ordered[:5]])
                    + ' ' + ' '.join([f'{s.fitness:.0f}' for s in ordered[5:]]))
                pbar.update()
            logger.info(f'{pbar.count} iterations in {pbar.elapsed:.2f}s')

    def iterate(self):
        no_improvement = 0
        prev_best = 0
        i = 0
        logger.info('Starting resolution...')
        for i in range(self.nbr_iterations):
            self.run()
            if self.nbr_no_improvement is not None:
                best = self.get_ordered()[0]
                if best.fitness > prev_best:
                    no_improvement = 0
                    prev_best = best.fitness
                else:
                    no_improvement += 1
                if no_improvement > 20:
                    break
            yield i

    def run(self):
        elite_nbr = round(self.elite_percent*self.size)
        ordered = self.get_ordered()
        # Progress bar of the solving
        with counter(total=self.size, desc='Solving', leave=False) as pbar:
            new_pop = ordered[:elite_nbr]
            pbar.update(len(new_pop))
            while len(new_pop) < self.size:
                sol1 = self.select_tournament()
                sol2 = self.select_tournament()
                new_sol = self.crossover(sol1, sol2)
                mut = new_sol.mutate()
                new_pop.append(mut)
                pbar.update()
        self.population = new_pop

    def create_from_power(self, F, P, r):
        """Get the flow rate from the power and radius."""
        # TODO: Move to another class or function
        fr = [0]*len(self.dt)
        T_k = p.T_r
        m = 975*2*pi*r**3
        A_s = 6*pi*r**2
        k_step = int(3600/p.delta_k)
        for i, t in enumerate(self.dt):
            for k in range(k_step):
                if F[i] > 0:
                    fr[i] += F[i]/(p.c_p_kW*(p.T_s - T_k))/k_step
                elif P[i] > 0:
                    fr[i] -= P[i]/(p.c_p_kW*(T_k - p.T_r))/k_step
                else:
                    fr[i] = 0
                losses = A_s*p.K_s*(T_k - p.T[t])
                T_k = (p.delta_k/(m*p.c_p_kW))*(F[i] - P[i] - losses) + T_k
            if fr[i] > p.dot_m_max:
                fr[i] = p.dot_m_max
            elif fr[i] < -p.dot_m_max:
                fr[i] = -p.dot_m_max
        return Individual(self, radius=r, flow_rate=fr)
