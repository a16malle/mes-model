#!/bin/bash

DIR="/home/antoine/ownCloud/1 Projects/Doctorat/5 Documents/2021-09 Article MES/article/img"

#python display_durations.py --nodisplay server/results/1080-168/* --out "$DIR/durations-winter.pdf"
#python display_pareto_convergence.py --nodisplay server/results/1080-168/* --out "$DIR/img/convergence-winter.pdf"
#python display_approximation.py --nodisplay server/results/1080-168/* --out "$DIR/error-winter.pdf"
#python display_objectives.py --nodisplay server/results/1080-168/* --out "$DIR/objectives-winter.pdf"

#python display_durations.py --nodisplay server/results/3936-168/* --out "$DIR/durations-summer.pdf"
#python display_pareto_convergence.py --nodisplay server/results/3936-168/* --out "$DIR/convergence-summer.pdf"
#python display_approximation.py --nodisplay server/results/3936-168/* --out "$DIR/error-summer.pdf"
#python display_objectives.py --nodisplay server/results/3936-168/* --out "$DIR/objectives-summer.pdf"

#python display_durations.py --nodisplay server/results/6288-168/* --out "$DIR/durations-mid.pdf"
#python display_pareto_convergence.py --nodisplay server/results/6288-168/* --out "$DIR/convergence-mid.pdf"
#python display_approximation.py --nodisplay server/results/6288-168/* --out "$DIR/error-mid.pdf"
#python display_objectives.py --nodisplay server/results/6288-168/* --out "$DIR/objectives-mid.pdf"

python display_durations.py --nodisplay server/results/0-8760-gap0.01/* --out "$DIR/durations-year-gap01.pdf"
python display_pareto_convergence.py --nodisplay server/results/0-8760-gap0.01/* --out "$DIR/convergence-year-gap01.pdf"
python display_approximation.py --nodisplay server/results/0-8760-gap0.01/* --out "$DIR/error-year-gap01.pdf"
python display_objectives.py --nodisplay server/results/0-8760-gap0.01/* --out "$DIR/objectives-year-gap01.pdf"

python display_durations.py --nodisplay server/results/0-8760-gap0.001/* --out "$DIR/durations-year-gap001.pdf"
python display_pareto_convergence.py --nodisplay server/results/0-8760-gap0.001/* --out "$DIR/convergence-year-gap001.pdf"
python display_approximation.py --nodisplay server/results/0-8760-gap0.001/* --out "$DIR/error-year-gap001.pdf"
python display_objectives.py --nodisplay server/results/0-8760-gap0.001/* --out "$DIR/objectives-year-gap001.pdf"
